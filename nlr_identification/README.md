[[_TOC_]] 

# NLR Identification

## Protein domain analysis 

#### InterProScan

1) Interproscan was used to identify the NBARC sequence, N-terminal domains, and the LRR domain using multiple analyses (Pfam,Gene3D,SUPERFAMILY,PRINTS,SMART,CDD,ProSiteProfiles).
2) The gff file for every species was filtered using the python script [**filternew.py**](../linked_scripts/filternew.py) to remove all entries without at least one NLR domain.
3) [**classifyinterproscan.R**](../linked_scripts/classifiyinterproscan.R) was written to use the domain annotations of interproscan to identify the NLRs and classify them into their subfamilies. 

The NLR counts/classifications can be found at: https://docs.google.com/spreadsheets/d/1ZZP0rSG9e8QofhWW7YU7fjnGUQZVOdwbC2NFLFg0u6I/edit#gid=983632573 

The total number of NLRs identified by Interproscan ranged from 114 to 569 (not including RNLS, 104 to 548 ) The average number of NLRs per species was 285 and the median was 251 (average of 270 and median of 235 without RNLs). Not including RNL and RN type, An average of 14% of the NLRs were complete (including N-terminal domain, NBARC, and LRR domain), ranging from 5% complete for *P. chiapensis* to 24% complete for *P. dalatensis*. 44.6% of the transcripts could not be classified as they were missing their N-terminal domain (either were NBARC only/NBS or NBARC-LRR/NL). This method identified very few CNLs or CC-type NLR, and more than half of the species had no CNLs or CN identified.

**Interproscan Example Script** 
```
#!/bin/bash
#SBATCH --job-name=interproscan_albicaulis
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load interproscan/5.35-74.0

/isg/shared/apps/interproscan/5.35-74.0/interproscan.sh -appl Pfam,Gene3D,SUPERFAMILY,PRINTS,SMART,CDD,ProSiteProfiles -i /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/final_transcriptomes/filtered_albicaulis_combined.aa -cpu 12
```

#### RGAugury

The RGAugury pipeline also uses interproscan for protein domain analysis after filtering the transcriptomes for likely NLRs using Blast similarity searches. 

The NLR counts/classifications can be found at: https://docs.google.com/spreadsheets/d/1ZZP0rSG9e8QofhWW7YU7fjnGUQZVOdwbC2NFLFg0u6I/edit#gid=0

The total number of NLRs identified ranged from 110 to 546 with an average of 274 and median of 242 NLRs per species. RGAugury identified more CNL type than interproscan, with an average of 11.7 and median of 8 CNLs per species, compared to an average less than 1 and median of 0 for interproscan. An average of 15% of the NLRs were complete, ranging from 6% for *P. chiapensis* to 27% for *P. dalatensis*. 47.3% of the transcripts could not be classified (missing N-terminal domain), which is similar to that of interproscan. No RNL type transcripts were identified, however, there were some NLRs labled as "other" coming out of RGAugury. 

**RGAugury Example Script**
```
#!/bin/bash
#SBATCH --job-name=RGAugury_albicaulis
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --exclude=xanadu-[05,10,11,13,14,15,16,17,19,25,26,30,51,61,62]
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load rgaugury/2019_02_12
export PFAMDB=/home/FCAM/abhattarai/database/pfam
export PATH=$PATH:/isg/shared/apps/rgaugury/2019_02_12
export COILSDIR=/isg/shared/apps/rgaugury/2019_02_12

perl /isg/shared/apps/rgaugury/2019_02_12/RGAugury.pl -p /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/rgaugury/rename_headers/nospace_albicaulis.aa -c 10
```

## NLRannotator 

NLRAnnotator was run on the cds sequences of the transcriptomes to identify additional potential NLRs.  The results were translated into the corresponding protein domains by the identified motif combinations to specify the N-terminal, NBARC, and LRR domains. The R script used for classification was [classifynlranntator.R.](../linked_scripts/classifynlranntator.R)

The NLR counts/classifications can be found at: https://docs.google.com/spreadsheets/d/1ZZP0rSG9e8QofhWW7YU7fjnGUQZVOdwbC2NFLFg0u6I/edit#gid=298649471

```
#!/bin/bash
#SBATCH --job-name=annotator_albicaulis
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

java -jar /home/FCAM/abhattarai/NLR-Annotator/ChopSequence.jar -i /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/final_transcriptomes/filtered_albicaulis_combined.cds -o /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/final_transcriptomes/albicaulis_chopped.fa

java -jar /home/FCAM/abhattarai/NLR-Annotator/NLR-Parser3.jar -t 4 -y /isg/shared/apps/meme/4.11.2_2/bin/mast -x /home/FCAM/abhattarai/NLR-Annotator/meme.xml -i /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/final_transcriptomes/albicaulis_chopped.fa -c albicaulis_nlr_cds.xml

java -jar /home/FCAM/abhattarai/NLR-Annotator/NLR-Annotator.jar -i albicaulis_nlr_cds.xml -o albicaulis_nlr_output.txt -g albicaulis_nlr_output.gff -b albicaulis_nlr_output.bed
```

## Final set of potential NLRs 

The results of Interproscan, RGAugury, and NLRannotator were combined using an R script to generate the final list of potential NLRs.
```
#!/bin/bash
#SBATCH --job-name=overlap_albicaulis
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=4G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date
module load R 

rga=/core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/rgaugury/albicaulis/core.NBS.candidates.lst
interpro=/core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/interproscan_method/classifying/albicaulis_annotations.txt
nlr=/core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/nlr_annotator_cds/albicaulis/nlr_class_albicaulis.txt 
outfile=combined_nlrs_albicaulis.tsv

Rscript merging_output_rga_int_nlr.R $rga $interpro $nlr $outfile

hostname
date
```
