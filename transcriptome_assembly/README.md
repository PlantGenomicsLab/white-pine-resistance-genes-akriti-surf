[[_TOC_]]

# Transcriptome Assembly

## Quality control 

Fastp was used for quality control and adapter trimming using defaults for quality and a minimum length of 50 bp. The following is an example script for *P. albicaulis*. Fastp was installed in my home directory.
```
#!/bin/bash
#SBATCH --job-name=fastptest
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

~/fastp -i header_SRR13823649_1.fastq -I header_SRR13823649_2.fastq -o fastp_SRR13823648_1.fastq.gz -O fastp_SRR13823648_2.fastq.gz -l 50 -h fastp_649.html -R SRR13823649_fastp --thread=4 
```

## Assembly Methods

Transcriptomes were assembled using Trinity, SOAPdenovo, and rnaSPAdes, and the Evigene pipeline was used for frame selection. The following are example scripts:

#### Trinity example script
```
#!/bin/bash
#SBATCH --job-name=srr649
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

module load trinity/2.8.5
module load samtools

Trinity --seqType fq --left //core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/dump/albicaulis/fastptest/fastp_SRR13823649_1.fastq --right /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/dump/albicaulis/fastptest/fastp_SRR13823649_2.fastq --min_contig_length 300 --CPU 16 --max_memory 100G --output trinity_SRR649 --full_cleanup
``` 
 
#### SOAPdenovo example script 
```
#!/bin/bash
#SBATCH --job-name=soapsubmit
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load SOAPdenovi-Trans/1.0.3

# Run with default k-mer value
SOAPdenovo-Trans-127mer all \
-s config.txt \
-o testalibicaulisout \
-p 16

echo -e "\nEnd time:"
date
```

For **SOAPdenovo** an example config file for one library:

```
#maximal read length
max_rd_len=500
[LIB]
#maximal read length in this lib
rd_len_cutof=45
#average insert size
avg_ins=120
#if sequence needs to be reversed 
reverse_seq=0
#in which part(s) the reads are used
asm_flags=3
#minimum aligned length to contigs for a reliable read location (at least 32 for short insert size)
map_len=32
#fastq file for read 1 
q1=/labs/Wegrzyn/akriti/dump/albicaulis/fastptest/fastp_SRR13823648_1.fastq
#fastq file for read 2 always follows fastq file for read 1
q2=/labs/Wegrzyn/akriti/dump/albicaulis/fastptest/fastp_SRR13823648_1.fastq
```

#### rnaSPAdes example script 
```
#!/bin/bash
#SBATCH --job-name=rnaspades
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load SPAdes/3.14.1

rnaspades.py \
-1 /labs/Wegrzyn/akriti/dump/albicaulis/fastptest/fastp_SRR13823648_1.fastq.gz \
-2 /labs/Wegrzyn/akriti/dump/albicaulis/fastptest/fastp_SRR13823648_2.fastq.gz \
-o testout

echo -e "\nEnd time:"
date
```


## Frame Selection and translating to protein sequences 

Following transcriptome assembly, each library needed to be formatted for Evigene using trformat.pl. This script was run once on each library/assembled transcriptome, and then the formatted files for each species (all libraries) were concatenated. After formatting, the script tr2aacds.pl from the evigene pipeline was used for frame selection and final transcript selection. Only the main output in the okayset was retained. Transcripts shorter than 300 bp were filtered out of the coding sequences file and the protein files were edited to match.

Formatting:

```
#!/bin/bash
#SBATCH --job-name=format_evigenetr
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

############################
#format rnaspades assembly #
############################

prefixdir=/labs/Wegrzyn/akriti/dump/albicaulis/evigene_assemblies/testout
prefix=rnaspades
filename=transcripts.fasta
outfile=rnaspades.tr

mkdir formatting
module load perl/5.28.1
module load blast/2.10.1
module load exonerate/2.4.0

evigene=/isg/shared/apps/evigene/20190101
export PATH=$evigene/scripts:$evigene/scripts/prot/:$evigene/scripts/rnaseq/:/isg/shared/apps/cdhit/4.6.8/:$PATH

trformat.pl -prefix $prefix -log \
-output formatting/$outfile \
-input $prefixdir/$filename

#############################
#format soapdenovo assembly #
#############################

prefixdir=/labs/Wegrzyn/akriti/dump/albicaulis/evigene_assemblies/soapdenovo
prefix=soapdenovo
filename=testalibicaulisout.scafSeq
outfile=soapdenovo.tr 

mkdir formatting
module load perl/5.28.1
module load blast/2.10.1
module load exonerate/2.4.0

evigene=/isg/shared/apps/evigene/20190101
export PATH=$evigene/scripts:$evigene/scripts/prot/:$evigene/scripts/rnaseq/:/isg/shared/apps/cdhit/4.6.8/:$PATH

trformat.pl -prefix $prefix -log \
-output formatting/$outfile \
-input $prefixdir/$filename

##########################
#format trinity assembly #
##########################

prefixdir=/labs/Wegrzyn/akriti/dump/albicaulis/evigene_assemblies/trinity
prefix=trinity
filename=trinity_SRR648.Trinity.fasta
outfile=trinity.tr 

mkdir formatting
module load perl/5.28.1
module load blast/2.10.1
module load exonerate/2.4.0

evigene=/isg/shared/apps/evigene/20190101
export PATH=$evigene/scripts:$evigene/scripts/prot/:$evigene/scripts/rnaseq/:/isg/shared/apps/cdhit/4.6.8/:$PATH

trformat.pl -prefix $prefix -log \
-output formatting/$outfile \
-input $prefixdir/$filename

cat rnaspades.tr soapdenovo.tr trinity.tr > totalformatted.tr

echo -e "\nEnd time:"
date
```

Frameselection:

```
#!/bin/bash
#SBATCH --job-name=tr2aa
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --exclude=xanadu-[05,10,11,13,14,15,16,17,19,25,26,30,51,61,62]
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

module load perl/5.28.1
module load blast/2.10.1
module load exonerate/2.4.0

evigene=/isg/shared/apps/evigene/20190101
export PATH=$evigene/scripts/:$evigene/scripts/prot/:$evigene/scripts/rnaseq/:/isg/shared/apps/cdhit/4.6.8/:$PATH

# Specify memory in MB, make sure it matches mem. in header

tr2aacds.pl -tidy \
-NCPU 16 \
-MAXMEM 80000 \
-log \
-cdna totalformatted.tr

echo -e "\nEnd time:"
date
```

Filtering: 
```
#!/bin/bash
#SBATCH --job-name=filtershort_albicaulis
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=16G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load seqtk

seqtk seq -L 300 /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/dump/albicaulis/evigene_assemblies/evigenetest/okayset/totalformatted.okay.cds > filtered_albicaulis_combined.cds

grep ">" filtered_albicaulis_combined.cds | sed 's/^>//g' > headers_filtered_albicaulis.txt

seqtk subseq /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/dump/albicaulis/evigene_assemblies/evigenetest/okayset/totalformatted.okay.aa headers_filtered_albicaulis.txt > filtered_albicaulis_combined.aa
```
