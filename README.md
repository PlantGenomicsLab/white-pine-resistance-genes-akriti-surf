[[_TOC_]]


testedit

# Introduction

**The White Pines**

The white pines consist of the subsection *Strobus* of the genus *Pinus*. Economically important species of white pine include the eastern white pine (*P. strobus*), western white pine (*P. monticola*), and sugar pine (*P. lambertiana*). Other non-commercially useful species such as limber pine (*P. flexilis*) and whitebark pine (*P. albicaulis*) are still ecologically important. Many of the white pines are susceptible to white pine blister rust (WPBR), a devastating invasive disease caused by the fungus *Cronartium ribicola*. 

**Identification of resistance genes**

White pines, including sugar pine, also exhibit resistance to WPBR through the contributions of many genes that confer partial resistance. Resistance (R) genes, also known as NLRs, often encode for a receptor that recognizes a specific pathogen-host interaction. Potential resistance genes can be annotated in plant genomes by identifying specific protein domains or sequence similarity to known resistance genes. 

**Goals of this project**

The purpose of this study is to identify and characterize putative resistance genes within white pine species using genome scanning and transcriptomic approaches.

***Spreadsheet*** The first contains SRA information and the second contains all NLR tables.

- https://docs.google.com/spreadsheets/d/11hppFDL1FJEwFY3R5-ya70WLRKeZH7Dljs8KeQ7dmXY/edit#gid=0
- https://docs.google.com/spreadsheets/d/1ZZP0rSG9e8QofhWW7YU7fjnGUQZVOdwbC2NFLFg0u6I/edit#gid=2087337721


**White pine species:**

- *P. albicaulis*
- *P. armandii*
- *P. ayacahuite*
- *P. bhutanica*
- *P. cembra*
- *P. chiapensis*
- *P. dabeshanensis*
- *P. dalatensis*
- *P. fenzeliana*
- *P. flexilis*
- *P. koraiensis*
- *P. kwangtungensis*
- *P. lambertiana*
- *P. monticola*
- *P. morrisonicola*
- *P. parviflora*
- *P. peuce*
- *P. pumila*
- *P. sibirica*
- *P. strobiformis*
- *P. strobus*
- *P. wallichiana*
- *P. wangii* 

Detailed SRA information can be found in the first linked spreadsheet. 

# Overview of Methods

## Transcriptome assembly 

Please see the README in the directory [transcriptome_assembly](transcriptome_assembly) for detailed methods and results as well as example scripts. The following is an overview/summary of the transcriptome assembly method:

1. Fastp was used for quality control and adapter trimming using defaults for quality and a minimum length of 50 bp.
2. Transcriptomes were assembled using Trinity, SOAPdenovo, and rnaSPAdes.
3. Assemblies were formatted using trformat.pl from Evigene and all formatted assemblies for each species were concatenated. The script tr2aacds.pl from the Evigene pipeline was used for frame selection and transcriptome filtering.
4. Transcripts shorter than 300 bp long were filtered out.

#### Transcriptome assessments 


| Species             | Total # | # > 500bp | # > 1kbp | avg length |  BUSCO                                |
| ------------------- | ------- | --------- | -------- | ---------- | ------------------------------------- |
| *P. albicaulis*     | 37586   | 15186     | 9447     | 744        | C:92.6%[S:88.8%,D:3.8%],F:1.4%,M:6.0% |
| *P. armandii*       | 60486   | 28593     | 12965    | 756        | C:88.2%[S:80.9%,D:7.3%],F:3.7%,M:8.1% |
| *P. ayacahuite*     | 29728   | 16952     | 9780     | 948        | C:88.4%[S:84.4%,D:4.0%],F:3.1%,M:8.5% |
| *P. bhutanica*      | 25417   | 15686     | 9403     | 1034       | C:91.1%[S:87.4%,D:3.7%],F:1.7%,M:7.2% |
| *P. cembra*         | 25310   | 15824     | 9252     | 1004       | C:85.9%[S:82.5%,D:3.4%],F:4.4%,M:9.7% |
| *P. chiapensis*     | 59474   | 26013     | 10911    | 705        | C:82.2%[S:79.6%,D:2.6%],F:4.3%,M:13.5%|
| *P. dabeshanensis*  | 27758   | 16339     | 9383     | 969        | C:89.7%[S:86.4%,D:3.3%],F:2.6%,M:7.7% | 
| *P. dalatensis*     | 28286   | 18663     | 11494    | 1079       | C:92.4%[S:88.4%,D:4.0%],F:1.9%,M:5.7% |
| *P. fenzeliana*     | 37241   | 20397     | 12014    | 945        | C:95.0%[S:90.2%,D:4.8%],F:1.1%,M:3.9% | 
| *P. flexilis*       | 25305   | 15583     | 9102     | 1013       | C:89.3%[S:85.3%,D:4.0%],F:2.4%,M:8.3% |
| *P. koraiensis*     | 24465   | 15391     | 9287     | 1045       | C:91.5%[S:87.3%,D:4.2%],F:1.5%,M:7.0% |
| *P. kwangtungensis* | 44861   | 22569     | 12038    | 849        | C:91.7%[S:85.6%,D:6.1%],F:1.8%,M:6.5% |
| *P. lambertiana*    | 25704   | 15762     | 9368     | 1017       | C:92.0%[S:88.4%,D:3.6%],F:1.3%,M:6.7% |
| *P. monticola*      | 27469   | 16905     | 10026    | 1013       | C:88.0%[S:84.1%,D:3.9%],F:3.3%,M:8.7% |
| *P. morrisonicola*  | 46269   | 23303     | 11038    | 797        | C:86.5%[S:82.8%,D:3.7%],F:3.3%,M:10.2%|
| *P. parviflora*     | 28044   | 17703     | 10263    | 1014       | C:91.2%[S:87.5%,D:3.7%],F:2.0%,M:6.8% |
| *P. peuce*          | 30280   | 19154     | 11446    | 1027       | C:93.4%[S:89.3%,D:4.1%],F:1.3%,M:5.3% |
| *P. pumila*         | 70597   | 29631     | 12876    | 708        | C:92.3%[S:88.0%,D:4.3%],F:1.8%,M:5.9% |
| *P. sibirica*       | 26390   | 15764     | 9049     | 956        | C:86.2%[S:83.1%,D:3.1%],F:4.0%,M:9.8% |
| *P. strobiformis*   | 26922   | 16048     | 9532     | 1001       | C:90.9%[S:86.7%,D:4.2%],F:1.7%,M:7.4% |
| *P. strobus*        | 25024   | 15833     | 9402     | 1036       | C:89.7%[S:85.9%,D:3.8%],F:2.4%,M:7.9% |
| *P. wallichiana*    | 25485   | 15944     | 9442     | 1029       | C:89.8%[S:86.1%,D:3.7%],F:2.2%,M:8.0% |
| *P. wangii*         | 27549   | 16862     | 9719     | 993        | C:89.8%[S:85.9%,D:3.9%],F:2.7%,M:7.5% |



## NLR Identification 

Please see the README in the directory [nlr_identification](nlr_identification/) for detailed methods, results, and example scripts. 

The results of InterProScan, RGAugury, and NLR Annotator were combined into one full set of [potential NLRs and their classifications.](https://docs.google.com/spreadsheets/d/1ZZP0rSG9e8QofhWW7YU7fjnGUQZVOdwbC2NFLFg0u6I/edit#gid=1460494032)


#### Protein Domain Analysis

Potential NLRs were identified based on conserved protein domains. Both Interproscan and RGAugury pipeline were used for protein domain analysis, and RGAugury integrates interproscan within its pipeline. The first difference is that RGAugury applies filtering steps before annotating protein domains with Interproscan, and the pipeline is limited to the CC, NBARC, LRR, and TIR domains to identify and classify NLRs. RNLs cannot be identified, but the benefit of RGAugury is that it is better able to identify coiled-coil domains, improving the identification and classification of CNL type NLRs. 


#### NLR Annotator 

Coding sequences were first processed with ChopSequence.jar from the [NLR annotator program](https://github.com/steuernb/NLR-Annotator) (Steuernagel et al., 2020). This does not actually chop any of the sequences because it is designed for segmenting genomes, but NLR annotator needed the sequences to be renamed according to its format or else it would cause issues with NLRparser NLRparser.jar was used to identify NLR associated motifs within the seqeunces using MAST from the MEME suite, and NLRannotator.jar was used to extract these motifs and determine combinations of motifs that indicate the NBARC domain, TIR or coiled coil domain, and the LRR domain. 


## Motif Discovery 

Please see the README in the directory [motifs](motifs/) for detailed methods, results, and example scripts.

783 Selected NLRs in conifer species (from [Van Ghelder et. al., 2019 ](https://www.nature.com/articles/s41598-019-47950-7)) were used with STREME from the MEME Suite to identify the major NBARC motifs. Rather than base MEME, STREME is recommended for discovering motifs in large sets of sequences. The output of STREME was edited to include the motif matrices for only the [8 NBARC-related motifs](motifs/streme_motifs.txt), and this was used with MAST to identify motifs in the potential white pines NLRs. The first and last motifs, the P-loop and QHD/MHD motif, were considered the start and end points of the NBARC domain, and this was used to extract just the NBARC region for further analysis. 


## Alignments and phylogenetic tree of NBARC domains

Please see the README in the directory [alignments](alignments/) for detailed methods, results, and example scripts. 

All NBARC regions and each orthogroup were aligned using Clustal Omega and manually edited using Geneious Prime. The resulting alignment was used to generate... 

## Orthogroups

Please see the README in the directory [orthogroups](orthogroups/) for detailed methods, results, and example scripts.

The full set of identified NLRs were seperated into orthogroups using Orthofinder. These orthogroups were then aligned using Clustal Omega and manually edited with Geneious Prime. A total of 24 orthogroups were identified, with the three largest representing the TNL, RNL, and CNL subfamilies. The TNL orthogroup was the largest with 713 transcripts, followed by RNL with 278, and finally CNLs with 114 transcripts. 







