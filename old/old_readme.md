[[_TOC_]]


# Introduction
*White pines*

The white pines consist of the subsection *Strobus* of the genus *Pinus*. Economically important species of white pine include the eastern white pine (*P. strobus*), western white pine (*P. monticola*), and sugar pine (*P. lambertiana*). Other non-commercially useful species such as limber pine (*P. flexilis*) and whitebark pine (*P. albicaulis*) are still ecologically important. Many of the white pines are susceptible to white pine blister rust (WPBR), a devastating invasive disease caused by the fungus *Cronartium ribicola*. 

*Identification of resistance genes*

White pines, including sugar pine, also exhibit resistance to WPBR through the contributions of many genes that confer partial resistance. Resistance (R) genes, also known as NLRs, often encode for a receptor that recognizes a specific pathogen-host interaction. Potential resistance genes can be annotated in plant genomes by identifying specific protein domains or sequence similarity to known resistance genes. 

*Goals of this project*

The purpose of this study is to identify and characterize putative resistance genes within white pine species using genome scanning and transcriptomic approaches.

***Spreadsheet***

- https://docs.google.com/spreadsheets/d/11hppFDL1FJEwFY3R5-ya70WLRKeZH7Dljs8KeQ7dmXY/edit#gid=0
- includes the SRA information for the samples used

## White pine species included in transcriptome analysis 
- pinus flexilis
- pinus albicaulis
- pinus monticola
- pinus strobus
- pinus lambertiana
- pinus armandii
- pinus bhutanica
- pinus dabeshanensis
- pinus fenzeliana
- pinus kwangtungensis
- pinus wallichiana
- pinus wangii
- pinus cembra
- pinus koraiensis
- pinus parviflora

# Sickle
Quality control of the SRA files was performed with sickle. The parameters were quality score of 25 and length 45. 

*information about sickle can be found here https://github.com/najoshi/sickle*

Scripts can be found in the white-pine-resistance-genes-akriti-surf/Sickle/ folder, scripts roughly group the samples together based on species.

```
module load sickle

for i in SRR4299096 SRR4299098 SRR4299099
do
sickle pe -t sanger -f "/home/FCAM/abhattarai/SURF_work/${i}_1.fastq.gz" -r "/home/FCAM/abhattarai/SURF_work/${i}_2.fastq.gz" -o "trimmed_${i}_1.fq.gz" -p "trimmed_${i}_2.fq.gz" -l 45 -q 25 -s "singles_${i}.fq.gz"
done
```

# Sample selection 
For species with many SRA libraries, the top three, based on number of paired reads post quality control with sickle, were chosen for transcriptome assembly using trinity. The spreadsheet contains more information on all the libraries.

| Pinus Species | SRA | Paired reads post sickle |
| ------ | ------ | ------ |
| albicaulis | SRR4299096 | 12581581 |
| albicaluis | SRR4299098 | 13236774 |
| albicaulis | SRR4299099 | 18391653 |
| armandii | SRR8585919 | 30539617 |
| armandii | SRR8585918 | 30805861 |
| armandii | SRR8585915 | 30934399 |
| bhutanica | SRR8585911 | 25269758 |
| bhutanica | SRR8585963 | 28799427 |
| bhutanica | SRR8585964 | 31038319 |
| cembra | ERR2936768 | 47980091 |
| cembra | ERR2936771 | 53331096 |
| cembra | ERR2936764 | 60792935 |
| dabeshanensis | SRR8585941 | 24768366 |
| fenzeliana | SRR8585966 | 30357040 |
| flexilis | SRR10032958 | 36219876 |
| flexilis | SRR10032967 | 37296301 |
| flexilis | SRR10032969 | 37580668 |
| koraiensis | SRR8357100 | 24877657 |
| koraiensis | SRR8357098 | 24973401 |
| kwangtungensis| SRR8585948 | 27569081 |
| kwangtungensis| SRR8585965 | 49740707 |
| monticola | SRR101383 | 53297481 |
| parviflora | ERR2040835 | 15084364 |
| strobus | SRR7175026 |  |
| wallichiana | SRR8585912 | 22528995 |
| wallichiana | SRR8585960 | 27189531 |
| wangii | SRR8585939 | 21441162 |
| wangii | SRR8585949 | 19531993 |


# Assembly with Trinity 
Transcriptomes were assembled using the de-novo assembler Trinity. De-novo means that there is no reference genome to guide the assembly of the transcriptome.
	
*more information can be found at https://github.com/trinityrnaseq/trinityrnaseq/wiki*
	
Scripts can be found in the white-pine-resistance-genes-akriti-surf/Trinity folder under each species name. 

Example script for Pinus albicaulis species:

```
module load trinity/2.6.6

for i in SRR4299096 SRR4299098 SRR4299099
do
Trinity --seqType fq --left "trimmed_${i}_1.fastq" --right "trimmed_${i}_2.fastq" --min_contig_length 300 --CPU 16 --max_memory 100G --output "trinity_${i}" --full_cleanup
done

```

## QUAST of trinity assemblies
Scripts can be found in the white-pine-resistance-genes-akriti-surf/Trinity folder under each species name.

Example script for Pinus albicaulis species:

```
module load rnaQUAST
module load GeneMarkS-T/5.1
rnaQUAST.py --transcripts trinity_SRR4299096.Trinity.fasta trinity_SRR4299098.Trinity.fasta trinity_SRR4299099.Trinity.fasta --threads 8 --gene_mark --output_dir .
```

| Pinus Species | SRA | Transcripts | Transcripts > 500 | Transcripts > 1000 | Transcript N50 |
| ------ | ------ | ------ | ------ | ------ | ------ |
| albicaulis | SRR4299096 | 58506 | 45823 | 45823 | 307 |
| albicaluis | SRR4299098 | 54791 | 41847 | 25915 | 456 |
| albicaulis | SRR4299099 | 58520 | 45146 | 28658 | 618 |
| armandii | SRR8585915 | 76382 | 57150 | 35589 | 1749 |
| armandii | SRR8585918 | 86224 | 64473 | 39533 | 1012 |
| armandii | SRR8585919 | 69211 | 52970 | 33160 | 3211 |
| bhutanica | SRR8585911 | 49435 | 40392 | 28418 | 1160 |
| bhutanica | SRR8585963 | 46928 | 39276 | 27908 | 996 |
| bhutanica | SRR8585964 | 47330  | 39939 | 29323 | 2321 |
| cembra | ERR2936764 | 213827 | 134061 | 70065 | 3735 |
| cembra| ERR2936768 | 196004  | 120438 | 60806 | 1517 |
| cembra | ERR2936771 | 183318  | 116288 | 60150 | 1418 |
| dabeshanensis | SRR8585941 | 62087 | 48327 | 30777 | 2267 |
| fenzeliana | SRR8585966 | 57158 | 45002 | 30550 | 1588 |
| flexilis | SRR10032958 | 98708 | 72507 | 45758 | 3207 |
| flexilis | SRR10032967 | 92592 | 69457 | 45075 | 1395 |
| flexilis | SRR10032969 | 103018 | 77252 | 48521 | 1086 |
| koraiensis | SRR8357098 | 56808 | 40539 | 24604 | 1603 |
| kwangtungensis| SRR8585948 | 59459 | 46507 | 30274 | 1356 |
| kwangtungensis| SRR8585965 | 63107 | 49874 | 32342 | 2042 |
| monticola | SRR101383 | 59993 | 41747 | 23912 | 1169 |
| parviflora | ERR2040835 | 52780 | 34920 | 17506 | 556 |
| strobus | SRR7175026 | 198218 | 129447 | 65426 | 1973 |
| wallichiana | SRR8585912 | 52977 | 41313 | 27662 | 3366 |
| wallichiana | SRR8585960 | 43031 | 36152 | 25931 | 3686 |
| wangii | SRR8585939 |  |  |  |  |
| wangii | SRR8585949 |  |  |  |  |

# Transdecoder 
the basic script for transdecoder was as follows:
```
module load TransDecoder/5.3.0
module load hmmer/3.2.1

for i in *.fasta
do
TransDecoder.LongOrfs -t "${i}"
hmmscan --cpu 16 --domtblout "${i}.domtblout" /isg/shared/databases/Pfam/Pfam-A.hmm "${i}.transdecoder_dir/longest_orfs.pep"
TransDecoder.Predict -t "${i}" --retain_pfam_hits "${i}.domtblout" --cpu 16
done
```
This failed to work for many of the species with more than one transdecoder file, especially the species *Pinus cembra*. Therefore, single scripts were made replacing the \*.fasta with the file names for each trinity output. These can be found in the respective species folders. For species with single samples/trinity outputs, the above script did work. 

Because of the way trinity assigns transcript ids/names, a prefix was added to distinguish transdecoder output before merging the files together (if there were multiple files for a species).

`sed 's/>/>1_/g' filename.cds > filename_prefix.cds`
`cat filename1.cds filename2.cds filename3.cds >> combined.cds`

# Clustering 
Vsearch was used to cluster after merging files as needed.

A sample script for clustering Pinus albicaulis is:

```
module load vsearch/2.4.3

vsearch --threads 8 --log LOGFile \
	--cluster_fast transdecoder/combined.cds \
	--id 0.90 \
	--centroids centroids.fasta \
	--uc clusters.uc
```

### RNA quast once per species
After clustering, rnaQUAST was run on each species. 

| species | total transcripts | transcripts > 500  | transcripts > 1000  | transcript N50 |
| ------ | ------ | ------ | ------ | ------ |
| albicaulis | 31376 | 19182 | 9186 |  |
| armandii | 36805 | 21884 | 11001 | 2049 |
| bhutanica | 25774 | 17107 | 9649 | 759 |
| cembra | | | | |
| dabeshanensis | 23165 | 15461 | 8027 | 1674 |
| fenzeliana |  |  |  |  |
| flexilis | 40382 | 22727 | 10926 | 321 |
| koraiensis | 29517 | 18214 | 9022 | 2391 |
| kwangtungensis | 28471 | 18730 | 10068 | 1869 |
| monticola | 25959 | 17202 | 8378 | 2928 |
| parviflora | 27627 | 16524 | 6763 | 501 |
| strobus | 51284 | 27812 | 12800 | 510 |
| wallichiana | 24405 | 16583 | 9339 | 6480 |
| wangii | 27087 | 17897 | 9670 | 1755 |

## Removing internals 
I returned to the original transdecoder output and remoded coding sequences flagged as internal (both 5' and 3' incomplete)

*createFasta.py can be found in the species folder*

```
grep -h ">" transdecoder/*.cds | tr -d '>' > cds_ids.txt
grep -h -v "internal" cds_ids.txt > non_internal.txt
python ../createFasta.py --fasta transdecoder/*.cds --nameList non_internal.txt --out filtered_non_internal.cds

```

### Classifying incompletes

incompletes and completes were characterized using 
`grep -c "3prime_partial" cds_ids.txt`
`grep -c "5prime_partial" cds_ids.txt`
`grep -c "complete" cds_ids.txt`


| species | 3 prime incompletes | 5 prime incompletes | completes |
| ------ | ------ | ------ | ------ |
| albicaulis| 38106 | 38788 | 144092 |
| armandii | 43124 | 50294 | 168956 |
| bhutanica | 8501 | 10603 | 75655 |
| cembra | 42258 | 78040 | 162876 |
| dabeshanensis | 4530 | 5725 | 25973 |
| fenzeliana | 2854 | 5470 | 26202 |
| flexilis | 41626 | 57230 | 224854 |
| koraiensis | 6726 | 6218 | 20867 |
| kwangtungensis | 7496 | 12102 | 53805 |
| monticola | 6746 | 8618 | 18101 |
| parviflora | 7721 | 7864 | 12933 |
| strobus | 13139 | 13139 | 47612 |
| wallichiana | 5628 | 8363 | 46468 |
| wangii | 8001 | 11387 | 47227 |


# Clustering (take 2)
The filtered output (without internals) was then clustered with an identity parameter of 0.95, to retain more unique coding sequences

```
module load vsearch/2.4.3

vsearch --threads 8 --log LOGFile \
	--cluster_fast transdecoder/combined.cds \
	--id 0.95 \
	--centroids centroids.fasta \
	--uc clusters.uc
```

## RNA QUAST of clustered output

| species | total transcripts | transcripts > 500  | transcripts > 1000  |
| ------ | ------ | ------ | ------ | 
| albicaulis | 32375 | 22295 | 10792 |
| armandii | 40444 | 25563 | 12688 | 
| bhutanica | 27010 | 18868 | 10685 | 
| cembra | 90338 | 45889 | 17165 |
| dabeshanensis | 22210 | 15736 | 8388 |
| fenzeliana | 20997 | 15478 | 9014 |
| flexilis | 43277 | 26440 | 12658 |
| koraiensis | 23606 | 16946 | 9130 |
| kwangtungensis | 29345 | 20298 | 10980 | 
| monticola | 21962 | 15948 | 8290 |
| parviflora | 20826 | 14215 | 6374 |
| strobus | 47151 | 27282 | 13190 |
| wallichiana | 24310 | 17486 | 9994 |
| wangii | 26823 | 18807 | 10401 |  


## BUSCO of clustered output

| Species | Results |
| ------ | ------ |
| albicaulis| C:74.8%[S:62.2%,D:12.6%],F:5.5%,M:19.7% |
| armandii | C:86.2%[S:73.8%,D:12.4%],F:3.0%,M:10.8% |
| bhutanica | C:89.3%[S:79.1%,D:10.2%],F:1.5%,M:9.2% |
| cembra |  |
| dabeshanensis | C:77.5%[S:70.8%,D:6.7%],F:7.0%,M:15.5% |
| fenzeliana | C:84.1%[S:77.8%,D:6.3%],F:4.2%,M:11.7% |
| flexilis | C:77.2%[S:67.3%,D:9.9%],F:1.4%,M:21.4% |
| koraiensis | C:75.7%[S:70.1%,D:5.6%],F:7.7%,M:16.6% |
| kwangtungensis | C:84.5%[S:75.8%,D:8.7%],F:4.0%,M:11.5% |
| monticola | C:68.6%[S:64.6%,D:4.0%],F:10.4%,M:21.0% |
| parviflora | C:52.3%[S:49.3%,D:3.0%],F:15.3%,M:32.4% |
| strobus | C:91.5%[S:83.1%,D:8.4%],F:2.2%,M:6.3% |
| wallichiana | C:87.3%[S:79.5%,D:7.8%],F:2.2%,M:10.5% |
| wangii | C:87.0%[S:77.6%,D:9.4%],F:3.3%,M:9.7% |

# Filter out short transcripts
Transcripts less than 350 were filtered out using the following scripts.

filterLength.py
```
import argparse
import os
from Bio import SeqIO

parser = argparse.ArgumentParser(
     prog='filterLength.py',
     usage='''python filterLength.py --fasta [fasta file] --length [minimum nucleotide length to keep] --out [name of output fasta file]''',
     description='''This program filters out sequences smaller than given length''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--fasta', type=str, help='The name of the fasta file', required=True)

parser.add_argument('--length', type=int, help='minimum nucleotide length to keep sequences', required=True)
parser.add_argument('--out', type=str, help='name of output fasta file', required=True)

args=parser.parse_args()
fasta=args.fasta
length=args.length
output=args.out

input_seq_iterator = SeqIO.parse(fasta, "fasta")
long_seq = [record for record in input_seq_iterator if len(record.seq) > length]

SeqIO.write(long_seq, output, "fasta")
```

***filterlength.sh***
```
python ../../filterLength.py --fasta centroids.fasta --length 350 --out centroids_350.fasta
```

## RNA QUAST and BUSCO on filtered set

rnaquast.sh
```
module load rnaQUAST/1.5.2
module load GeneMarkS-T/5.1

rnaQUAST.py --transcripts ../vsearch2/centroids_350.fasta --gene_mark --threads 8 -o results
```
busco.sh
```
module load busco/4.0.2
module load hmmer/3.2.1
module load blast

busco -m transcriptome -i vsearch2/centroids_350.fasta -o output_busco3 -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -f
```

| species | transcripts| transcripts > 500 | transcripts > 1000 | BUSCO |
| ------ | ------ | ------ | ------ | ------ |
| albicaulis | 28903 | 22494 | 10877 | C:75.5%[S:62.9%,D:12.6%],F:5.6%,M:18.9% |
| armandii | 34492 | 25702 | 12747 | C:86.6%[S:74.0%,D:12.6%],F:3.0%,M:10.4 | 
| bhutanica | 23830 | 18868 | 10685 | C:89.3%[S:79.1%,D:10.2%],F:1.4%,M:9.3% |  
| cembra | 72113 | 45889 | 17165 | C:89.8%[S:75.9%,D:13.9%],F:2.7%,M:7.5% |
| dabeshanensis | 19886 | 15736 | 8388 | C:77.2%[S:70.6%,D:6.6%],F:6.9%,M:15.9% |
| fenzeliana | 18916 | 15478 | 9014 | C:83.9%[S:77.8%,D:6.1%],F:4.1%,M:12.0% |
| flexilis | 36936 | 26811 | 12850 | C:78.7%[S:68.5%,D:10.2%],F:1.4%,M:19.9% |
| koraiensis | 21307 | 16946 | 9130 | C:75.5%[S:69.9%,D:5.6%],F:7.7%,M:16.8% |
| kwangtungensis | 25865 | 20298 | 10980 | C:84.5%[S:75.8%,D:8.7%],F:4.0%,M:11.5% |
| monticola | 19820 | 15948 | 8290 | C:68.5%[S:64.6%,D:3.9%],F:10.2%,M:21.3% |
| parviflora  | 18462 | 14215 | 6374 | C:52.1%[S:49.1%,D:3.0%],F:15.1%,M:32.8% |
| strobus | 39004 | 27282 | 13190 | C:91.2%[S:82.8%,D:8.4%],F:2.3%,M:6.5% |
| wallichiana | 21740 | 17486 | 9994 | C:87.1%[S:79.4%,D:7.7%],F:2.2%,M:10.7% |
| wangii | 23637 | 18807 | 10401 | C:86.9%[S:77.5%,D:9.4%],F:3.2%,M:9.9% |

# Samples to keep for downstream analysis 
based on BUSCO cutoff of 80% and at least 20,000 transcripts
- armandii
- bhutanica
- cembra
- kwangtungensis
- lambertiana
- strobus
- wallichiana
- wangii

### Infomation for selected species
| species | transcripts| transcripts > 500 | transcripts > 1000 | BUSCO |
| ------ | ------ | ------ | ------ | ------ |
| armandii | 34492 | 25702 | 12747 | C:86.6%[S:74.0%,D:12.6%],F:3.0%,M:10.4 | 
| bhutanica | 23830 | 18868 | 10685 | C:89.3%[S:79.1%,D:10.2%],F:1.4%,M:9.3% |  
| cembra | 72113 | 45889 | 17165 | C:89.8%[S:75.9%,D:13.9%],F:2.7%,M:7.5% |
| kwangtungensis | 25865 | 20298 | 10980 | C:84.5%[S:75.8%,D:8.7%],F:4.0%,M:11.5% |
| strobus | 39004 | 27282 | 13190 | C:91.2%[S:82.8%,D:8.4%],F:2.3%,M:6.5% |
| wallichiana | 21740 | 17486 | 9994 | C:87.1%[S:79.4%,D:7.7%],F:2.2%,M:10.7% |
| wangii | 23637 | 18807 | 10401 | C:86.9%[S:77.5%,D:9.4%],F:3.2%,M:9.9% |

# Filtering pep file to match cds file
To then run analysis on the protein domains the pep file output from transdecoder needed to be filtered to match the cds file after clustering and filtering by length >350

The following script was used to filter the pep files:
```
for i in albicaulis armandii bhutanica cembra dabeshanensis fenzeliana flexilis koraiensis kwangtungensis monticola parviflora strobus wallichiana wangii 
do 
cd /labs/Wegrzyn/Akriti_SURF/species
cd ${i}
sed 's/\s.*$//g' transdecoder/combined.pep > renamed.pep
echo '#!/bin/bash
#SBATCH --job-name=filterpep
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o filterpep%j.out
#SBATCH -e filterpep%j.err

grep -h ">" vsearch2/centroids_350.fasta | tr -d ">" > cds_headers.txt
sort cds_headers.txt > sorted_cds_headers.txt
python ../createFasta.py --fasta renamed.pep --nameList sorted_cds_headers.txt --out filtered_350_pep.pep' > filtering_pep.sh
sbatch filtering_pep.sh
done
```

# InterProScan
I ran InterProScan on the filtered pep files, using the Pfam and SMART databases.
The following script was used to run across the selected species:

```
for i in armandii bhutanica cembra kwangtungensis strobus wallichiana wangii
do 
cd /labs/Wegrzyn/Akriti_SURF/species
cd ${i}
mkdir interproscan_pep
cd interproscan_pep
echo '#!/bin/bash
#SBATCH --job-name=interproscan_pep
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load interproscan/5.35-74.0

/isg/shared/apps/interproscan/5.35-74.0/interproscan.sh -appl Pfam,SMART -i ../filtered_350_pep.pep' > interproscan.sh
sbatch interproscan.sh
done 
```

## Filtering InterProScan output 
The output from InterProScan was filtered to extract the sequences that contain Pfam domains of interest (NB-ARC, TIR, RPW8, Rx). 

```
import os
import re
import argparse

parser = argparse.ArgumentParser(
    prog='filtergff.py',
    usage='''python filtergff.py --gff [gff file given by InterProScan]''',
    description='''this program pulls out specific entries from the gff file corresponding to features of NLR genes''')
parser.add_argument('--gff', type=str, help='the name of the gff file', required=True)
args=parser.parse_args()
gff=args.gff

open_gff = open(gff, "r")
content = open_gff.read()
content_list = content.split("##")
open_gff.close()
gff_only = content_list[4:-2]
fasta_only = content_list[-1:]

pf_family = ('PF00931','PF01582','PF05659', 'PF18052', 'SM00255')
filter_list = []
for item in gff_only:
    for pf in pf_family:
        if pf in item:
            x = item
            filter_list.append(x)
len(filter_list)
filter_list2 = list(dict.fromkeys(filter_list))
len(filter_list2)

id_list = []
for item in filter_list:
    filter_split = item.split(" ")
    new_id = filter_split[1]
    id_list.append(new_id)
len(id_list)
id_list2 = list(dict.fromkeys(id_list))
len(id_list2)

with open('new_gff_filtered.gff3', 'w') as filehandle:
    for listitem in filter_list2:
        filehandle.write('##' + listitem)

with open('new_transcript_ids.txt', 'w') as filehandle:
    for listitem in id_list2:
        filehandle.write('%s\n' % listitem)
```

## Characterizing Interproscan Results
I used the filtered protein domain output from interproscan to identify complete and partial resistance genes. 

Using the Pfam and SMART databases, only two known resistance gene N-terminal domains were identified (TIR and RPW8). Therefore, CNLs are not included in the categorization, but the transcripts labeled as NLRs could contain coiled-coil N-terminal domains. 

Based on the available domain information, transcripts were categorized as:
- Complete:
	- NLR (no known N-terminal domain, just NB-ARC + LRR)
	- TNL (TIR + NB-ARC + LRR)
	- RNL (RPW8 + NB-ARC + LRR)

- Incomplete:
	- TN (TIR + NB-ARC)
	- RN (RPW8 + NB-ARC)
	- TX (TIR only)
	- RX (RPW8 only)
	
I used the follwing python script to count the occurance of each type of resistance gene structure:
```
#!/usr/bin/env python
# coding: utf-8

import sys
import argparse

parser = argparse.ArgumentParser(
    prog='Characterizing_interproscan_results.py',
    usage='''python Characterizing_interproscan_results.py --gff [gff file given by InterProScan]''',
    description='''this program categorizes interproscan results based off features of NLR genes''')
parser.add_argument('--gff', type=str, help='the name of the gff file', required=True)
args=parser.parse_args()
gff=args.gff

with open(gff,'r') as file:
    results=file.read()
    results_split = results.split('##')

potential_completes = []
non_NBS = []
for item in results_split:
    if 'NB-ARC' in item:
        x=item
        potential_completes.append(x)
    else:
        z=item
        non_NBS.append(z)

NBS_LRR = []
NBS = []
for item in potential_completes:
    if 'Leucine' in item:
        x=item
        NBS_LRR.append(x)
    else: 
        z = item 
        NBS.append(z)
		
TNL = []
RNL = []
NLR = []
for item in NBS_LRR:
    if 'TIR' in item:
        x = item
        TNL.append(x)
    elif 'RPW8' in item:
        y = item 
        RNL.append(y)
    else:
        z = item
        NLR.append(z)
TN = []
RN = []
NBS_only = []
for item in NBS:
    if 'TIR' in item:
        x = item 
        TN.append(x)
    elif 'RPW8' in item:
        y = item
        RN.append(y)
    else:
        z = item
        NBS_only.append(z)
TX = []
RX = []
other = []
for item in non_NBS:
    if 'TIR' in item:
        x = item 
        TX.append(x)
    elif 'RPW8' in item:
        z = item
        RX.append(z)
    else:
        y = item
        other.append(y)

running_list = []
for item in NLR:
    a = item.split(' ')
    b = a[1] + '\tNLR'
    running_list.append(b)
for item in TNL:
    c = item.split(' ')
    d = c[1] + '\tTNL'
    running_list.append(d)
for item in RNL:
    e = item.split(' ')
    f = e[1] + '\tRNL'
    running_list.append(f)
for item in TN:
    a = item.split(' ')
    b = a[1] + '\tTN'
    running_list.append(b)
for item in RN:
    c = item.split(' ')
    d = c[1] + '\tRN'
    running_list.append(d)
for item in TX:
    e = item.split(' ')
    f = e[1] + '\tTX'
    running_list.append(f)
for item in RX:
    a = item.split(' ')
    b = a[1] + '\tRX'
    running_list.append(b)
for item in other:
    c = item.split(' ')
    d = c[1] + '\tother'
    running_list.append(d)

with open('interproscan_transcriptid_type.txt', 'w') as filehandle:
    for listitem in running_list:
        filehandle.write('%s\n' % listitem)
		
results = 'Completes:' + '\nNLR:' + str(len(NLR)) + '\nTNL:' + str(len(TNL)) + '\nRNL:' + str(len(RNL))\
+ '\nIncompletes:''\nTN:' + str(len(TN)) + '\nRN:' + str(len(RN)) + '\nTX:' + str(len(TX)) + '\nRX:' + str(len(RX)) + '\nOther:' + str(len(other))

original_stdout = sys.stdout # Save a reference to the original standard output
with open('Interproscan_results_characterization.txt', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print(results)
    sys.stdout = original_stdout # Reset the standard output to its original value
```

#### Interproscan results table 

| Species                 | armandii | bhutanica  | cembra | kwangtungensis | lambertiana  | strobus  | wallichiana | wangii |
|-------------------------|----------|------------|--------|----------------|--------------|----------|-------------|--------|
| NLR                     | 3        | 3          | 38     | 19             | 89           | 39       | 5           | 8      |
| TNL                     | 1        | 3          | 4      | 4              | 49           | 2        | 0           | 5      |
| RNL                     | 0        | 0          | 0      | 0              | 1            | 0        | 0           | 0      |
| NBS                     | 61       | 51         | 374    | 67             | 391          | 268      | 56          | 65     |
| TN                      | 20       | 21         | 119    | 61             | 113          | 99       | 34          | 30     |
| RN                      | 7        | 17         | 19     | 18             | 31           | 23       | 12          | 15     |
| TX                      | 31       | 31         | 236    | 65             | 123          | 161      | 40          | 42     |
| RX                      | 6        | 1          | 10     | 3              | 6            | 4        | 4           | 8      |
| other                   | 0        | 0          | 8      | 2              | 0            | 2        | 2           | 2      |
| Total from interproscan | 129      | 127        | 808    | 239            | 803          | 598      | 153         | 175    |



# Orthofinder
Orthofinder was run on the filtered pep files using the following script:
```
#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o orthofinder%j.out
#SBATCH -e orthofinder%j.err

module load OrthoFinder/2.3.3
module unload diamond
module load diamond/0.9.25

orthofinder -f pep_files_2/ -S diamond -t 10
```
The directory pep_files_2 contains all of the pep files.

# RGAugury 
RGAugury followed the process of analysis using only protein sequences. RGAaugury outputs information about resistance gene analogs including NLRs and Pattern recognition receptors (PRRs), which are involved in the basal response. 

The following is an example of the script used to run RGAugury:
```
#!/bin/bash
#SBATCH --job-name=rga
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=40G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x%j.out
#SBATCH -e %x%j.err

module load rgaugury/2019_02_12
export PFAMDB=/home/FCAM/abhattarai/Pfam_database
export PATH=$PATH:/isg/shared/apps/rgaugury/2019_02_12
export COILSDIR=/isg/shared/apps/rgaugury/2019_02_12

perl /isg/shared/apps/rgaugury/2019_02_12/RGAugury.pl -p /labs/Wegrzyn/Akriti_SURF/species/armandii/filtered_350_pep_sans_ast.pep -c 10

```

## RGAugury results
I have reported only the results for NLRs type. RGA outputs a summary of sequence types, including:
- NBS without a leading domain: 
- CNLs (coiled-coil, NBS, LRR)
- TNLs (TIR, NBS, LRR)
- CN (coiled-coil, NBS)
- TN (TIR, NBS)
- NL (NBS, LLR)
- TX (TIR only)

| SPECIES | NBS | CNL | TNL | CN | TN | NL | TX | OTHERS |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| armandii | 45 | 0 | 8 | 6 | 21 | 16 | 21 | 3 |
| bhutanica | 32 | 4 | 15 | 6 | 13 | 30 | 31 | 2 |
| cembra | 294 | 21 | 29 | 42 | 114 | 81 | 227 | 11 |
| kwangtungensis | 54 | 5 | 20 | 11 | 51 | 34 | 60 | 1 |
| lambertiana | 217 | 50 | 116 | 33 | 62 | 226 | 115 | 29 |
| strobus | 204 | 22 | 24 | 27 | 90 | 89 | 143 | 16 |
| wallichiana | 41 | 4 | 15 | 10 | 19 | 22 | 42 | 4 |
| wangii | 49 | 7 | 14 | 8 | 23 | 29 | 40 | 3 |


### RGAugury and interproscan overlap

The following script filters the RGA and interproscan outputs based on transcripts identified by both. RGAaugury also uses interproscan to identify resistance genes. The major differences are that RGAugury utilizes Blast to filter transcripts based on sequence similarity to known resistance genes (from PRGdb) and also uses ncoils to identify coiled-coiled domains, which my interproscan process does not. 

### Getting the final set of resistance gene transcripts from the Interproscan and RGAugury programs

```
#!/bin/bash
#SBATCH --job-name=RGA_interproscan
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

for i in armandii bhutanica cembra kwangtungensis wallichiana wangii
do

#copying over the files to the RGA_interproscan_overlap folder
cd /labs/Wegrzyn/Akriti_SURF/species/${i}
mkdir RGA_interproscan_overlap
cd RGA_interproscan_overlap
cp ../interproscan_pep/interproscan_transcriptid_type.txt .
cp ../RGA/*.NBS.candidates.lst .

#keeping only the transcript name and not the species part
awk 'BEGIN{OFS = "\t"; regex = "[0-9][0-9a-zA-Z_]+\.[a-z][0-9]";}{if(match($1,regex)){pattern = substr($1,RSTART,RLENGTH); print pattern, $2}}' interproscan_transcriptid_type.txt > interproscan_sans_species.txt

sort interproscan_sans_species.txt > sorted_interproscan.txt
sort *NBS.candidates.lst > sorted_RGA.txt

#using join to find the overlapping and not overlapping portions
join sorted_interproscan.txt sorted_RGA.txt > RGA_interproscan.txt
join -v 1 sorted_interproscan.txt sorted_RGA.txt > interproscan_only.txt
join -v 2 sorted_interproscan.txt sorted_RGA.txt > RGA_only.txt

#make sure the interproscan NLRs are turned to NL to keep notation consistent
awk 'sub(/NLR/,"NL",$2)' RGA_interproscan.txt 

#Match the RGA and interproscan outputs, keep the RGA output unless it is NBS, in that case is likely RN or RX
awk 'BEGIN {OFS = "\t";}
{if ($2 == $3){print $1,$2} else {if ($3 == "NBS"){print $1,$2} else {print $1,$3}}}' RGA_interproscan.txt > matching.txt

#to add the results from interproscan only and RGA only
cat matching.txt interproscan_only.txt RGA_only.txt > possible_final_set.txt

cd /labs/Wegrzyn/Akriti_SURF/species

done

#lambertiana specific
for i in lambertiana 
do

#copying over the files to the RGA_interproscan_overlap folder
cd /labs/Wegrzyn/Akriti_SURF/species/${i}
mkdir RGA_interproscan_overlap
cd RGA_interproscan_overlap
cp ../interproscan_pep/interproscan_transcriptid_type.txt .
cp ../RGA/*.NBS.candidates.lst .

sort interproscan_transcriptid_type.txt > sorted_interproscan.txt
sort *NBS.candidates.lst > sorted_RGA.txt

#using join to find the overlapping and not overlapping portions
join sorted_interproscan.txt sorted_RGA.txt > RGA_interproscan.txt
join -v 1 sorted_interproscan.txt sorted_RGA.txt > interproscan_only.txt
join -v 2 sorted_interproscan.txt sorted_RGA.txt > RGA_only.txt

#make sure the interproscan NLRs are turned to NL to keep notation consistent
awk 'sub(/NLR/,"NL",$2)' RGA_interproscan.txt 

#Match the RGA and interproscan outputs, keep the RGA output unless it is NBS, in that case is likely RN or RX
awk 'BEGIN {OFS = "\t";}
{if ($2 == $3){print $1,$2} else {if ($3 == "NBS"){print $1,$2} else {print $1,$3}}}' RGA_interproscan.txt > matching.txt

#to add the results from interproscan only and RGA only
cat matching.txt interproscan_only.txt RGA_only.txt > possible_final_set.txt

cd /labs/Wegrzyn/Akriti_SURF/species

done


#strobus specific
for i in strobus 
do

#copying over the files to the RGA_interproscan_overlap folder
cd /labs/Wegrzyn/Akriti_SURF/species/${i}
mkdir RGA_interproscan_overlap
cd RGA_interproscan_overlap
cp ../interproscan_pep/interproscan_transcriptid_type.txt .
cp ../RGA/*.NBS.candidates.lst .

#keeping only the transcript name and not the species part
sed 's/strobus_//g' interproscan_transcriptid_type.txt > interproscan_sans_species.txt

sort interproscan_sans_species.txt > sorted_interproscan.txt
sort *NBS.candidates.lst > sorted_RGA.txt

#using join to find the overlapping and not overlapping portions
join sorted_interproscan.txt sorted_RGA.txt > RGA_interproscan.txt
join -v 1 sorted_interproscan.txt sorted_RGA.txt > interproscan_only.txt
join -v 2 sorted_interproscan.txt sorted_RGA.txt > RGA_only.txt

#make sure the interproscan NLRs are turned to NL to keep notation consistent
awk 'sub(/NLR/,"NL",$2)' RGA_interproscan.txt 

#Match the RGA and interproscan outputs, keep the RGA output unless it is NBS, in that case is likely RN or RX
awk 'BEGIN {OFS = "\t";}
{if ($2 == $3){print $1,$2} else {if ($3 == "NBS"){print $1,$2} else {print $1,$3}}}' RGA_interproscan.txt > matching.txt

#to add the results from interproscan only and RGA only
cat matching.txt interproscan_only.txt RGA_only.txt > possible_final_set.txt

cd /labs/Wegrzyn/Akriti_SURF/species

done

```

| species        | total number of interproscan transcripts  | total number of RGA transcripts | total number of overlapping transcripts |
|----------------|-------------------------------------------|---------------------------------|-----------------------------------------|
| armandii       | 129                                       | 126                             | 123                                     |
| bhutanica      | 127                                       | 133                             | 126                                     |
| cembra         | 808                                       | 819                             | 791                                     |
| kwangtungensis | 239                                       | 236                             | 234                                     |
| lambertiana    | 803                                       | 848                             | 796                                     |
| strobus        | 598                                       | 615                             | 593                                     |
| wallichiana    | 153                                       | 157                             | 147                                     |
| wangii         | 175                                       | 173                             | 166                                     |


The characterization of the potential final set of genes (Interproscan only, RGA only, and their overlapping transcripts). 

| species | total | NLR | TNL | RNL | CNL | NBS | TN | RN | CN | TX | RX | other |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| armandii | 132 | 16 | 8 | 0 | 0 | 39 | 21 | 6 | 6 | 27 | 6 | 3 |
| bhutanica | 137 | 30 | 15 | 0 | 4 | 27 | 13 | 5 | 6 | 31 | 1 | 5 |
| cembra | 836 | 81 | 29 | 0 | 21 | 286 | 114 | 9 | 42 | 227 | 10 | 17 |
| kwangtungensis | 241 | 34 | 20 | 0 | 5 | 45 | 51 | 9 | 11 | 60 | 3 | 3 |
| lambertiana | 855 | 226 | 116 | 0 | 50 | 207 | 62 | 11 | 33 | 115 | 6 | 29 |
| strobus | 620 | 89 | 24 | 0 | 22 | 190 | 90 | 14 | 27 | 143 | 4 | 17 |
| wallichiana | 163 | 22 | 15 | 0 | 4 | 36 | 19 | 5 | 10 | 42 | 4 | 6 |
| wangii | 182 | 29 | 14 | 0 | 7 | 41 | 23 | 8 | 8 | 40 | 7 | 5 |


# NLR annotator
### Run 1

NLR annotator can only be run on a genome, which we have for Pinus lambertiana. There are three steps to NLR annotator, chopping the genome FASTA into overlapping subsequences, searching the subsequences for NLR associated motifs, and integration of the identified motifs and searching for the NLR loci. 

NLR annotator can be found at https://github.com/steuernb/NLR-Annotator. I used the branch nlr_parser3 as I had a newer version of the MEME suite. 

The genome was chopped into 20kb segments with a 5kb overlap. The script for chopping the genome sequence:
```
module load java
module load meme/4.11.2_2

java -jar /home/FCAM/abhattarai/NLR_annotator3/ChopSequence.jar -i /isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/pila.v1.5.provisional.masked.fasta -o pila_masked_chopped.fasta -l 20000 -p 5000
```
The script for NLR parser which identifies NLR associated motifs:
```
module load java

java -jar /home/FCAM/abhattarai/NLR_annotator3/NLR-Parser3.jar -t 16 -y /isg/shared/apps/meme/4.11.2_2/bin/mast -x /home/FCAM/abhattarai/NLR_annotator3/meme.xml -i /labs/Wegrzyn/Akriti_SURF/species/lambertiana/pila_masked_chopped.fasta -c pila.nlr.xml
```
NLR annotation was performed with the default parameters for distance w/in motif combination (500bp), distance for elongating (2500bp), and the distance between motif combinations (10kb). The script for NLR annotator:
```
module load java

java -jar /home/FCAM/abhattarai/NLR_annotator3/NLR-Annotator.jar -i pila.nlr.xml -o pila.nlr.output.txt -g pila.nlr.output.gff
```
NLR annotator classifies the output of annotated NLRs as complete or incomplete. Possible NLRs are labeled as complete if they contain motif 1, at least one of motifs 9, 11, or 19, and one of the possible combinations: (1,6,4) (1,4,5) (6,4,5) (4,5,10) (5,10,3) (10,3,12) (3,12,3). The program also labels predicted NLRs as psuedogenes if there is a predicted stop codon within the annotated region. This does not mean that the NLR is a pseudogene as the stop codon might fall in an intron. NLRs not annotated as pseudogenes may also be psuedogenes. 

The following figure from Steuernagel et al. summarizes the process of NLR Annotator (Steuernagel et al., 2020)

![alt text](image/NLR_ann_fig1.jpg "Figure 1 (Steuernagel et al., 2020)")
 
### Run 2
The genome was chopped into 200kb segments with 10kb overlap
```
module load java
module load meme/4.11.2_2

java -jar /home/FCAM/abhattarai/NLR_annotator3/ChopSequence.jar -i /isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/pila.v1.5.provisional.masked.fasta -o pila_masked_chopped.fasta -l 200000 -p 10000
```
NLR parser was run on the chopped genome sequence using the same script as in NLR Annotator run 1.

NLR annotation was run with the following parameters:
- distance w/in motif combination: 2500 bp 
- distance for elongating: 10 kb 
- distance between motif combinations: 100 kb 

The script for NLR annotator:
```
module load java

java -jar /home/FCAM/abhattarai/NLR_annotator3/NLR-Annotator.jar -i pila.nlr2.xml -o pila.nlr.output2.txt -g pila.nlr.output2.gff -b pila.nlr.output2.bed -distanceWithinMotifCombination 2500 -distanceForElongating 10000 -distanceBetweenMotifCombinations 100000
```
## Processing NLR Annotator output 

**Replacing complete (pseudogene) with complete_pseudogene and partial (pseudogene) with partial_pseudogene**

`sed -i.bak -e 's/complete (pseudogene)/complete_pseudogene/g' -e 's/partial (pseudogene)/partial_pseudogene/g' pila.nlr.output.txt `


**Adding columns to make gff format for use with bedtools intersect**

`awk 'BEGIN{OFS="\t"}{$5=$5 OFS "."}{$6=$6 OFS "."}{print $0}' pila.nlr.output.txt > pila_nlr_txt2gff.gff3`


**Using bedtools intersect to find the overlap between NLRs and genes**
```
srun --partition=general --qos=general --mem=5G --pty bash
module load bedtools
bedtools intersect -wo -a pila_nlr_txt2gff.gff3 -b /labs/Wegrzyn/Akriti_SURF/species/lambertiana/Pila.1_5.gtf > NLR_overlapping_genes_all_elements.txt
#to get the part overlapping just genes
bedtools intersect -wo -a pila_nlr_txt2gff.gff3 -b /labs/Wegrzyn/Akriti_SURF/species/lambertiana/Pila_genes_only.gtf > NLR_overlapping_genes.txt
```


**Finding the overlap percentage of NLRs and genes**

`awk 'BEGIN{OFS="\t";} function abs(v) {return (v < 0) ? -v : v }{g_len = $14 - $13; n_len = $5 - $4; p_gene = $19 / g_len; p_nlr = $19 / n_len;}{print $0,n_len,g_len,p_nlr,p_gene}' NLR_overlapping_genes.txt > percent_overlaps.txt`


**Creating a file with just the completes, and also calculating the number of complete and complete_pseudogene**
```
grep complete NLR_overlapping_genes.txt > complete_NLR.txt
var1="$(grep '\s''complete''\s' complete_NLR.txt | wc -l)"
var2="$(grep complete_pseudogene complete_NLR.txt | wc -l)"
echo "number of completes $var1
number of complete_pseudogene $var2"
```

**Finding the NLRs that overlap more than one gene, and calculating the number of complete/complete_pseudogene**
```
awk 'a[$2]++{if(a[$2]>1){ print b}; print $0} {b=$0}' complete_NLR.txt > duplicates.txt
var3="$(grep '\s''complete''\s' duplicates.txt | wc -l)"
var4="$(grep complete_pseudogene duplicates.txt | wc -l)"
echo "number of completes overlapping more than one gene $var3
number of complete_pseudogene overlapping more than one gene $var4"
```

**Getting the distribution of %gene overlap and %NLR overlap for completes**
```
module load R
library(ggplot2)
library(plyr)
library(dplyr)

po <- read.table("completes_only_percent_overlaps.txt",sep = "\t")
g_by_type=group_by(po,V3)

pdf(file="/labs/Wegrzyn/Akriti_SURF/species/lambertiana/NLR1_files/overlap_lengh_distribution.pdf")

stat_tbl<- g_by_type %>% summarize("mean_NLR_percentage"=mean(V22),
                       "sd_NLR_percentage"=sd(V22),
                       "median_NLR_percentage"=median(V22),
                       "mean_gene_percentage"=mean(V23),
                       "sd_gene_percentage"=sd(V23),
                       "median_gene_percentage"=median(V23))



ggplot(data=g_by_type, aes(x=V22, group=V3, fill=V3))+geom_boxplot()+labs(x="portion of NLR coverage", title="Overlap as portion of NLR coverage")
ggplot(data=g_by_type, aes(x=V22, group=V3, color=V3))+geom_density()+labs(x="portion of NLR coverage",title="Distribution of overlap as portion of NLR coverage")

ggplot(data=g_by_type, aes(x=V23, fill=V3))+geom_boxplot()+labs(x="portion of gene coverage", title = "Overlap as portion of gene coverage")
ggplot(data=g_by_type, aes(x=V23, group=V3, color=V3))+geom_density()+labs(x="portion of gene coverage", title="Overlap as portion of gene coverage")

dev.off()
```

### Transcripts identified by both Interproscan and NLR annotator 

```
#!/usr/bin/env python
# coding: utf-8

with open('/labs/Wegrzyn/Akriti_SURF/species/lambertiana/interproscan/new_gff_filtered.gff3', 'r') as fn:
    ips = fn.read()
    ips_split = ips.split('##')

with open('NLR_overlapping_genes.txt', 'r') as fn2:
    genes_NLR_overlap = fn2.readlines()
	
interproscan_entries_matching_NLRs_overlapping_genes = []
interproscan_entries_matching_NLRs_overlapping_genes = []
interproscan_entries_only = []
NLR_entries_only = []

for gene in genes_NLR_overlap:    
    gene_split = gene.split('\t')
    for item in ips_split:
        if gene_split[17] in item:
            string = "\t".join(gene_split)
            x = (string + item)
            y = item
            z = string
            interproscan_entries_matching_NLRs_overlapping_genes.append(x)
            interproscan_entries_only.append(y)
            NLR_entries_only.append(z)

with open('interproscan_entries_matching_NLRs_overlapping_genes.txt', 'w') as filehandle:
    for listitem in interproscan_entries_matching_NLRs_overlapping_genes:
        filehandle.write(listitem)
		
#each item is a single NLR/Interproscan match 
print(len(interproscan_entries_matching_NLRs_overlapping_genes))
```

### NLR Run 1 Results

| Type | Count |
| ------ | ------ |
|total number of NLRs identified | 1828  |
| complete NLRS |  972 |
| Completes overlapping genes | 613 |
| Completes overlapping more than one gene | 55 |i
| NLRs overlapping interproscan results (includes partials) | 686 |


### NLR Run 2 Results 

| Type | Count |
| ------ | ------ |
|total number of NLRs identified | 2335  |
| complete NLRS | 1211  |
| Completes overlapping genes | 986 |
| Completes overlapping more than one gene | 81 |
| NLRs overlapping interproscan results (includes partials) | 714 |



## Analysis of orthofinder results:

**re-ran orthofinder with species tree**

The species tree output from Orthofinder was:

![alt text](image/orthofinder_tree.png "Orthofinder species tree output")

However it should be more like:
![alt text](image/subgenus_strobus.jpg "species tree")


Orthofinder requires a binary tree, so the assumed species tree file was as shown:

![alt text](image/user_tree.png "user defined species tree")


**scripts to get key information from orthofinder results**

1. filtering orthogroups with identified NLR genes
2. getting overall species distributions of theses orthogroups
3. getting NLR counts of these orthogroups
4. getting longest genes/transcripts of these orthogroups
5. annotating orthogroups based on longest transcript
6. compiling all this information into one document


**Steps 1,2 and 3**

Orthogroups were filtered and the species and NLR counts for the retained orthogroups were calculated using the python script: 

[Orthofinder](orthofinder/Orthofinder_filtering_Oct20_2020.ipynb)

As I ran into issues using my original script on the HPC due to pandas not being found/not available, for the time being I ran the script on my computer until I could find a work around. I moved my Orthofinder results file *Orthogroups.txt* and the file with identified NLR transcript IDs and their classifications*combined_transcript_ids.txt* over to my computer and ran the script there before moving the output files back over to the cluster.

The output files are:
- Orthogroups_filtered.txt
- Orthogroups_filtered_ids.txt
- Orthogroups_filtered_species_dist.txt
- Orthogroups_filtered_NLR_dist.txt

Each orthogroup had a colon after the name, therefore these were stripped using sed for each of these files
```
sed 's/\://g' Orthogroups_filtered.txt > Orthogroups_filtered2.txt
sed 's/\://g' Orthogroups_filtered_ids.txt > Orthogroups_filtered_ids2.txt
sed 's/\://g' Orthogroups_filtered_species_dist.txt > Orthogroups_filtered_species_dist2.txt
sed 's/\://g' Orthogroups_filtered_NLR_dist.txt > Orthogroups_filtered_NLR_dist2.txt
```
When run using windows this added '\r' therefore the files end of line characters were stripped using:
```
tr -d '\r' < Orthogroups_filtered_NLR_dist2.txt > Orthogroups_filtered_NLR_dist_3.txt
tr -d '\r' < Orthogroups_filtered_species_dist2.txt > Orthogroups_filtered_species_dist3.txt
```

**Steps 4 and 5**

The longest transcript/gene for each orthogroup was determined using the following python script: [get_longest.py] (orthofinder/analysis/py_scripts/get_longest.py)

```
### Load up those output files from orthofinder and entap
with open("/labs/Wegrzyn/Akriti_SURF/orthofinder/analysis/analysis3_11_2021/Orthogroups_filtered2.txt", "r") as genegroups:
    ggroups = genegroups.read().splitlines()

#### calculate mean and stdev for number in each orthogroup for the angiosperm trees
#### also convert the number in each orthogroup for the neuropterans to int from string objects
import re
from operator import itemgetter
#from ast import literal_eval

# # ##### Calculate the gene lengths (aa length) for each gene
f = open("/labs/Wegrzyn/Akriti_SURF/orthofinder/pep_files_2/combined_pep.fasta", "r")
content = f.read()
lengths = []
seqList = re.split(">", content)
del seqList[0]
for seq in seqList:
    header, sequence = seq.split("\n",1)
    lengths.append([header,len(sequence)])
# sort longest genes first
sorted_lengths = sorted(lengths, key=itemgetter(1), reverse=True)
#print(sorted_lengths)

# # ### get a list of the longest gene for each orthogroup
oglong = open("orthogroup_longest.txt" , "w")
for og in ggroups:
    genes = re.split("\t|, ", og)

    def getLongest():
        for l in sorted_lengths:
            if l[0] in genes:
                return l[0]
				
    if genes[0] != "" and genes[0] != "Orthogroup":   
        l = getLongest()
        if l is not None:
            oglong.write(genes[0] + "\t" + l + "\n")
oglong.close()

```


***
Minor problems with these methods, need to update filenames and paths 

***


**Step 6**

The files were all combined together using join 

```
join --header -j 1 -t $'\t' Orthogroups_filtered_species_dist3.txt Orthogroups_filtered_NLR_dist3.txt > joined_species_NLR.txt

```

### Updates:

Reran the orthofinder filtering on the newest version of the orthofinder runs but the methods were the same
I noticed that many of the orthogroups had many genes but only one was identified as an NLR in my analysis. That makes me concerned because I would expect at least a handful of the genes to be identified as an NLR in some way. There are no unexpected patterns in what type of NLR is identified or the species distributions. Only one of the orthogroups was found almost entirely in one species (orthogroup 9), which I saw before too. The transcripts identified are mostly NL or NBS, followed by TN, TNL, and TX. This is expected because incomplete NLRs and the TIR family were the most common. 



## MEME analysis of Orthogroups

For the orthogroup motif analysis, I wanted to have at least 20 transcripts in each orthogroup. I sorted the orthogroups by number of transcripts and took the ID of those that had at least 20 transcripts. There were 48 orthogroups I selected, and I put their orthogroup IDs into a list in the file 'filt_20.txt'. I then used this ID list to select these orthogroups from the filtered orthogroup file (that contained only the orthogroups that had been identified to contain resistance genes). This file is called 'orthogroups_filt_20.txt'. Each of the 48 lines contains an orthogroup, starting with the orthogroup ID and followed by the transcript ID of each transcript in the orthogroup. I then wanted to write each orthogroup to its own file so that I could use those transcript IDs. I did this using *split* to seperate "orthogroups_filt_20.txt" by line. Split stores this in a series of new files with a common base name. There were now 48 files with one line each, and this line contained the orthogroup ID and the transcript IDs seperated by spaces. These spaces were then replaced with new lines so that the transcript IDs were on their own lines and could be used with the getFasta.sh script to pull the protein sequences from the overall FASTA file into per-orthogroup FASTA files.

```
grep -F -f filt_20.txt ../Orthogroups_filtered.txt > orthogroups_filt_20.txt
split -l 1 -d orthogroups_filt_20.txt og20_

ogname=$(find og20*)

for i in $ogname
do
tr [:space:] '\n' < ${i} > list_${i}.txt
done
```

**getFasta.sh**

```
#!/bin/bash
#SBATCH --job-name=getfasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

listogname=$(find list_og20*)

for i in $listogname
do 
python /labs/Wegrzyn/Akriti_SURF/meme/createFasta.py --fasta combined_pep.fasta --path /labs/Wegrzyn/Akriti_SURF/orthofinder/pep_files_2 --nameList ${i} --out pep_${i}.fasta
done 
```

These files were then used for the motif analysis. 

## Motif Analysis

For analysis using MEME suite the protien fasta files were filtered to include only the NLR sequences

```
for i in armandii bhutanica cembra kwangtungensis lambertiana strobus wallichiana wangii
do 
cd /labs/Wegrzyn/Akriti_SURF/species/${i}/RGA_interproscan_overlap/
awk '{print $1}' possible_final_set.txt > possible_final_trans_only.txt
echo '#!/bin/bash
#SBATCH --job-name=filtering_pep
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=8G
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
'"
echo `hostname`
python /labs/Wegrzyn/Akriti_SURF/species/createFasta.py --fasta /labs/Wegrzyn/Akriti_SURF/species/${i}/filtered_350_pep_sans_ast.pep --nameList possible_final_trans_only.txt --out ${i}_possible_final_trans.pep
" > filtering_pep.sh
sbatch filtering_pep.sh
cd /labs/Wegrzyn/Akriti_SURF/species
done 
```

To determine the NBARC motifs MEME was ran using the following script:

```
for i in armandii bhutanica cembra kwangtungensis lambertiana strobus wallichiana wangii
do 
echo '#!/bin/bash
#SBATCH --job-name='"${i}_meme"'
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
'"
echo `hostname`
module load meme/5.0.5

meme ${i}_possible_final_trans.pep -minsites 5 -maxsites 50 -nmotifs 10 -oc ${i} -plib /isg/shared/apps/meme/5.0.5/share/meme-5.0.5/prior30.plib -protein 
" > ${i}_meme.sh
sbatch ${i}_meme.sh
done
```

Multiple alignment was also performed using

```
#!/bin/bash
#SBATCH --job-name=multipleseq
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load muscle/3.8.31

muscle -in armandii_possible_final_trans.pep -out armandii_alignment.afa

```

However, this did not yield the expected results. The results from MEME yielded one motif for each species, rather than multiple motifs as expected. 

### Motif analysis by orthogroup 
Protein FASTA files for each orthogroup with 20 or more transcripts were analyzed using a newer version of MEME (5.2.0). The results can be found at /labs/Wegrzyn/Akriti_SURF/meme/ortho/ortho20/meme2 

The following script is an example for the first orthogroup, there are scripts for each of the 48 orthogroups with > 20 transcripts (excluding 2 orthogroups that were likely did not contain resistance genes and were mistakenly sorted as an NLR orthogroup. MEME was first run with a max motif width of 30 aa, and then run with a max motif width of 50 to see if MEME could capture some of the missing NBS motifs. Unfortunately this did not work, and another run with a maximum of 10 motifs will be run.  Once this is done, a report with the top 15 motifs and their classifications will be generated.

```
#!/bin/bash
#SBATCH --job-name=pep_list_og20_00.txt.fasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo hpc-ext-4.cam.uchc.edu
export PATH=/home/FCAM/abhattarai/meme/bin:/home/FCAM/abhattarai/meme/libexec/meme-5.2.0:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

meme -protein -nmotifs 10 -minw 5 -maxw 30 -oc output_pep_list_og20_00.txt.fasta /labs/Wegrzyn/Akriti_SURF/meme/ortho/ortho20/pep_files/pep_list_og20_00.txt.fasta
```

### Motif analysis  by NLR type 
Protien FASTA files for each NLR type (CNL, RNL, TNL, NBS) were analyzed using MEME (5.2.0). The results can be found in the folders output_l200_cnl.fa, output_l200_nbs.fa, output_l200_rnl.fa, output_l200_tnl.fa at /labs/Wegrzyn/Akriti_SURF/meme/id_lists

A summary and the classifications of the top 15 motifs found in each NLR type can be found at https://docs.google.com/spreadsheets/d/1y01np7Y_7PdKClrtY_DQvSUhDMjSYSnisRi3q_THy-E/edit?usp=sharing

An example script is below. Setting the max number of motifs to 15 identified more of the essential NBS motifs than 10 max motifs. The essential motifs were identified clearly in the CNL and NBS runs, but not in the RNL and TNL runs. This may be because these files contained many transcripts with just the RPW8 and TIR n-terminal domains, causing MEME to identify n-terminal domain specific motifs and possibly confusing the program when it tries to identify the NBS domain motifs because so many of the transcripts are missing this domain. The RNL category also has the least number of transcripts, decreasing the number of NBS domains for MEME to analyze.


```
#!/bin/bash
#SBATCH --job-name=meme_l200_cnl.fa
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo xanadu-11.cam.uchc.edu
export PATH=/home/FCAM/abhattarai/meme/bin:/home/FCAM/abhattarai/meme/libexec/meme-5.2.0:/isg/shared/apps/seqtk/1.2:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

meme -protein -nmotifs 15 -minw 5 -maxw 30 -oc output_l200_cnl.fa /labs/Wegrzyn/Akriti_SURF/meme/id_lists/l200_cnl.fa
```


### extracting motifs from MEME files for orthofinder MEME analysis

The python script uses BioPython to parse the MEME output files, it extracts the motif information and can be used to list all of the identified motifs as well as information on the motifs if desired. Here I just extracted the motif consensus sequence 

```
#!/usr/bin/env python3.8.1
# coding: utf-8

from Bio import motifs

with open("meme.txt") as handle:
    record = motifs.parse(handle, "meme")
    
motifs = []

for motif in record:
    motifs.append(motif.consensus)

with open('motifs_list.txt', 'w') as file:
    for item in motifs:
        file.write('%s\n' % item)
```

The python script was run in each output folder resulting from MEME, which contains the "meme.txt" file for each orthogroup run through MEME. Therefore one motifs_list.txt was created in each folder. The bash script was:

```
#!/bin/bash
#SBATCH --job-name=testing_motif_extraction
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


module load python/3.8.1

outputfolder=$(find output_pep_list* -type d)


for i in $outputfolder
do 
cd /labs/Wegrzyn/Akriti_SURF/meme/ortho/ortho20/meme3/${i}
python3 /labs/Wegrzyn/Akriti_SURF/meme/ortho/ortho20/motif_extraction.py
done
```

The motif analysis did not seem to reveal anything useful, as some of the motifs could not be identified by MEME, and otherwise the motifs followed with the already identified type of NLR. 



# Overview/updates
## 8/12/21

### NLR related 
- NLR variation within species and between species 
- various copy numbers 
- using the 2021 limber pine paper's canditate gene sequence for Cr4, I found that PILA 23126 is the ortholog in the Pila 1.5v genome. One section had an identity of 93% for the best hit to this gene, and one other hit around 90% to the same scaffold but around 800,000 rather than 500,000, which is probably from a paralog. They had characterized this gene as a TNL type, but I was only able to characterize it as an NB-ARC containing sequence using interproscan. NLR annotator I found that it did characterize this as a complete TNL. However, NLR annotator annotated the TNL from 524652 to 536942, where the genome annotation, the gene spans from 500601 to 526024. Only the last CDS of the gene is included in the NLR annotation.

### Challenges with transcriptomes
- I really am noticing fragmentation and wierd alignments when trying to align more complete and partial NLRs together, like I was doing for each orthogroup to see if the gene tree matched that determined by orthofinder 
- Is the difference in number of NLRs identified per species due to copy number variation or an inability to identify all the potential NLR transcripts 
- I've noticed that if any motifs are duplicated within some sequences or if the longest transcript is much longer than most of the transcripts that also affects the overal alignment

### Orthogroup/orthofinder related
- Would it be worthwhile to look if other gene families or orthogroups (besides NLRs) are expanded or shared by just the Asian species? I saw in one paper that NLR activity/expression is also associated with salicylic acid production/signalling. Is there is an easy way to check this or would it even be useful/worthwhile? 
- There seem to be a lot of potential paralogs in each orthogroup, and the high number of NLR cembra transcripts assigned to each orthogroup seem to be mostly 'isoforms' of the same 'gene' identified by Trinity
-Is clustering at 0.95 causing a significant difference in NLR transcript number? From the number of transcripts before and after clustering in each species, it doesn't look like it is affecting the overall transcript number much, but since NLRs are known to have many copies and possible paralogs, do you think there would be a significant difference between using 0.95 or 0.98 identity for clustering. When using their NLR p. strobus sequences to blast against my transcriptomes, I belive there were a handful of transcripts that had 100% identity but most were much lower. 
- I've gone through alignments of some of the top orthogroups, and I am noticing that many of the transcripts really are incomplete, and the alignments have a lot of gaps.

### Motif related
- I was able to determine that STREME is the better program for motif discovery in terms of sensitivity and accuracy, but it was giving me shorter motifs and increasing the minimum motif size may exclude some motifs as STREME would identify them
- MEME is also recommended if there are around 50 sequences or less, which applies to the CNL2 group in the conifer NLRs paper, as well as my orthogroups, while STREME is recommended for more than 50 sequences
- I was unable to use the descriminatory motifs (RNBS-D and the MDH/QHD motifs) to classify transcripts as either CNL, RNL, or TNL, as 1) I was unable to find both motifs for the CNL2 class and 2) when using  MAST and FIMO for motif scanning, the program was either unable to deal with the similarity of the motifs and gave a warning, or sequences were being labeled with motifs from different subfamilies (like CNL2 and RNL or TNL and CNL).





# New Transcriptome Assemblies 

White pine species:

- *P. albicaulis*
- *P. armandii*
- P. ayacahuite
- P. cembra
- P. chiapensis
- P. dalatensis
- P. fenzeliana
- P. flexilis
- P. koraiensis
- P. kwangtungensis
- P. lambertiana
- P. monticola
- P. morrisonicola
- P. parviflora
- P. peuce
- P. pumila
- P. sibirica
- P. strobiformis
- P. strobus

Detailed SRA information can be found on the spreadsheet linked at the top of this page. 



## TRANSCRIPTOME ASSESSMENTS FOLLOWING EVIGENE AND FILTERING OUT TRANS. SHORTER THAN 300bp. 


| Species          | Total # | # > 500bp | # > 1kbp | avg length |  BUSCO                                |
| ---------------- | ------- | --------- | -------- | ---------- | ------------------------------------- |
| *P. albicaulis*  | 37586   | 15186     | 9447     | 744        | C:92.6%[S:88.8%,D:3.8%],F:1.4%,M:6.0% |
| *P. armandii*    | 60486   | 28593     | 12965    | 756        | C:88.2%[S:80.9%,D:7.3%],F:3.7%,M:8.1% |
| P. ayacahuite    | 29728   | 16952     | 9780     | 948        | C:88.4%[S:84.4%,D:4.0%],F:3.1%,M:8.5% |
| P. cembra        | 25310   | 15824     | 9252     | 1004       | C:85.9%[S:82.5%,D:3.4%],F:4.4%,M:9.7% |
| P. chiapensis    | 59474   | 26013     | 10911    | 705        | C:82.2%[S:79.6%,D:2.6%],F:4.3%,M:13.5%|
| P. dalatensis    | 28286   | 18663     | 11494    | 1079       | C:92.4%[S:88.4%,D:4.0%],F:1.9%,M:5.7  |
| P. fenzeliana    | 37241   | 20397     | 12014    | 945        | C:95.0%[S:90.2%,D:4.8%],F:1.1%,M:3.9% | 
| P. flexilis      | 25305   | 15583     | 9102     | 1013       | C:89.3%[S:85.3%,D:4.0%],F:2.4%,M:8.3% |
| P. koraiensis    | 24465   | 15391     | 9287     | 1045       | C:91.5%[S:87.3%,D:4.2%],F:1.5%,M:7.0% |
| P. kwangtungensis| 44861   | 22569     | 12038    | 849        | C:91.7%[S:85.6%,D:6.1%],F:1.8%,M:6.5% |
| P. lambertiana   | 25704   | 15762     | 9368     | 1017       | C:92.0%[S:88.4%,D:3.6%],F:1.3%,M:6.7% |
| P. monticola     | 27469   | 16905     | 10026    | 1013       | C:88.0%[S:84.1%,D:3.9%],F:3.3%,M:8.7% |
| P. morrisonicola | 46269   | 23303     | 11038    | 797        | C:86.5%[S:82.8%,D:3.7%],F:3.3%,M:10.2%|
| P. parviflora    | 28044   | 17703     | 10263    | 1014       | C:91.2%[S:87.5%,D:3.7%],F:2.0%,M:6.8% |
| P. peuce         | 30280   | 19154     | 11446    | 1027       | C:93.4%[S:89.3%,D:4.1%],F:1.3%,M:5.3% |
| P. pumila        | 70597   | 29631     | 12876    | 708        | C:92.3%[S:88.0%,D:4.3%],F:1.8%,M:5.9% |
| P. sibirica      | 26390   | 15764     | 9049     | 956        | C:86.2%[S:83.1%,D:3.1%],F:4.0%,M:9.8% |
| P. strobiformis  | 26922   | 16048     | 9532     | 1001       | C:90.9%[S:86.7%,D:4.2%],F:1.7%,M:7.4% |
| P. strobus       | 25024   | 15833     | 9402     | 1036       | C:89.7%[S:85.9%,D:3.8%],F:2.4%,M:7.9% |















