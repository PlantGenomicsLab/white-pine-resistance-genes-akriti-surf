#!/bin/bash
#SBATCH --job-name=fastqc
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

module load fastqc


fastqc ERR2936764_1.adj.fastq ERR2936764_2.adj.fastq ERR2936768_1.adj.fastq ERR2936768_2.adj.fastq ERR2936771_1.adj.fastq ERR2936771_2.adj.fastq 
