#!/bin/bash
#SBATCH --job-name=transdecoder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=50G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

module load TransDecoder/5.3.0
module load hmmer/3.2.1

for i in trinity__ERR2936764.Trinity.fasta
do
TransDecoder.LongOrfs -t "${i}"
done

