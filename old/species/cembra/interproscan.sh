#!/bin/bash
#SBATCH --job-name=interproscan
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load interproscan/5.35-74.0

/isg/shared/apps/interproscan/5.35-74.0/interproscan.sh -appl Pfam,SMART -i ../filtered_350_pep.pep
