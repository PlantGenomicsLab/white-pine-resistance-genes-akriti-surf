#!/bin/bash
#SBATCH --job-name=filterFasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=35G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o filterFasta_%j.out
#SBATCH -e filterFasta_%j.err

hostname
echo "\nStart time:"
date

grep -h ">" transdecoder/*.cds | tr -d '>' > cds_ids.txt
grep -h -v "internal" cds_ids.txt > non_internal.txt
python ../createFasta.py --fasta transdecoder/*.cds --nameList non_internal.txt --out filtered_non_internal.cds

echo "\nEnd time:"
date
