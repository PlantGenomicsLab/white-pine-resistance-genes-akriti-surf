#!/bin/bash
#SBATCH --job-name=filterpep
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o filterpep%j.out
#SBATCH -e filterpep%j.err

grep -h ">" vsearch2/centroids_350.fasta | tr -d ">" > cds_headers.txt
sort cds_headers.txt > sorted_cds_headers.txt
python ../createFasta.py --fasta renamed.pep --nameList sorted_cds_headers.txt --out filtered_350_pep.pep
