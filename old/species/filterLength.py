import argparse
import os
from Bio import SeqIO

parser = argparse.ArgumentParser(
     prog='filterLength.py',
     usage='''python filterLength.py --fasta [fasta file] --length [minimum nucleotide length to keep] --out [name of output fasta file]''',
     description='''This program filters out sequences smaller than given length''',
     epilog='''It requires numpy and biopython libraries''')
parser.add_argument('--fasta', type=str, help='The name of the fasta file', required=True)

parser.add_argument('--length', type=int, help='minimum nucleotide length to keep sequences', required=True)
parser.add_argument('--out', type=str, help='name of output fasta file', required=True)

args=parser.parse_args()
fasta=args.fasta
length=args.length
output=args.out

input_seq_iterator = SeqIO.parse(fasta, "fasta")
long_seq = [record for record in input_seq_iterator if len(record.seq) > length]

SeqIO.write(long_seq, output, "fasta")

