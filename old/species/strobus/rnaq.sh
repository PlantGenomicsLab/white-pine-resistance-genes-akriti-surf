#!/bin/bash
#SBATCH --job-name=quast
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=50G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load rnaQUAST
module load GeneMarkS-T/5.1

for i in *.fasta
do
rnaQUAST.py --transcripts "${i}" --threads 8 --gene_mark --output_dir . 
done

