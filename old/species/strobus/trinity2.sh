#!/bin/bash
#SBATCH --job-name=trinity
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

module load trinity/2.6.6

Trinity --seqType fq --left /home/FCAM/abhattarai/SURF_work/easternwhitepine/ewp_1.fastq --right /home/FCAM/abhattarai/SURF_work/easternwhitepine/ewp_2.fastq --min_contig_length 300 --CPU 16 --max_memory 100G --output trinity_ewp --full_cleanup

