#!/bin/bash
#SBATCH --job-name=filterFasta
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=35G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o filterFasta_%j.out
#SBATCH -e filterFasta_%j.err


python ../../filterLength.py --fasta centroids.fasta --length 350 --out centroids_350.fasta
