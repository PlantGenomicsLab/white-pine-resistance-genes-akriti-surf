#!/bin/bash
#SBATCH --job-name=nlrannotator
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load java

java -jar /home/FCAM/abhattarai/NLR_annotator3/NLR-Annotator.jar -i pila.nlr.xml -o pila.nlr.output.txt -g pila.nlr.output.gff

