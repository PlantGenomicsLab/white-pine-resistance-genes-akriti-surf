#!/bin/bash
#SBATCH --job-name=nlrparser
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load java

java -jar /home/FCAM/abhattarai/NLR_annotator3/NLR-Parser3.jar -t 16 -y /isg/shared/apps/meme/4.11.2_2/bin/mast -x /home/FCAM/abhattarai/NLR_annotator3/meme.xml -i /labs/Wegrzyn/Akriti_SURF/species/lambertiana/pila_masked_chopped.fasta -c pila.nlr.xml

