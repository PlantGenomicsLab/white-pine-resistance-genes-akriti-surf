#!/bin/bash
#SBATCH --job-name=Chopsequence
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load java
module load meme/4.11.2_2

java -jar /home/FCAM/abhattarai/NLR_annotator3/ChopSequence.jar -i /isg/shared/databases/alignerIndex/plant/Pila/genome/v1.5/pila.v1.5.provisional.masked.fasta -o pila_masked_chopped.fasta -l 20000 -p 5000

