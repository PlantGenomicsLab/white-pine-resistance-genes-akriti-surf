#!/bin/bash
#SBATCH --job-name=trinity_kwang
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

module load trinity/2.6.6

for i in SRR8585948 SRR8585965
do
Trinity --seqType fq --left "trimmed_${i}_1.fastq" --right "trimmed_${i}_2.fastq" --min_contig_length 300 --CPU 16 --max_memory 100G --output "trinity_${i}" --full_cleanup
done

