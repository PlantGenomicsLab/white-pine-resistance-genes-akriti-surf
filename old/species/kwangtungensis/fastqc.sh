#!/bin/bash
#SBATCH --job-name=fastqc
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

module load fastqc


fastqc trimmed_trimmed_SRR8585948_1.fastq trimmed_SRR8585948_2.fastq trimmed_SRR8585965_1.fastq trimmed_SRR8585965_2.fastq

