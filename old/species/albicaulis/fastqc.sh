#!/bin/bash
#SBATCH --job-name=fastqc
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

module load fastqc

for i in SRR4299096 SRR4299098 SRR4299099
do
fastqc "trimmed_${i}_1.fastq" "trimmed_${i}_2.fastq"
done

