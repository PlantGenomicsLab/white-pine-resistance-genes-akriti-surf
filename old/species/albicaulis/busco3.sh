#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=30G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load busco/4.0.2
module load hmmer/3.2.1
module load blast

busco -m transcriptome -i vsearch2/centroids_350.fasta -o output_busco3 -l /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -f
