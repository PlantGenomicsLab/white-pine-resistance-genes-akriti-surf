#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

module load busco/4.0.2
module load hmmer/3.2.1
module load blast

busco -m transcriptome -i vsearch2/centroids.fasta -o output_busco2 -l  /isg/shared/databases/BUSCO/odb10/embryophyta_odb10 -f
