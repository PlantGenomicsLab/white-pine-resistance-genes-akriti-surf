#!/bin/bash
#SBATCH --job-name=transdecoder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mem=50G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`

module load TransDecoder/5.3.0
module load hmmer/3.2.1

for i in *.fasta
do
TransDecoder.LongOrfs -t "${i}"
hmmscan --cpu 16 --domtblout "${i}.domtblout" /isg/shared/databases/Pfam/Pfam-A.hmm "${i}.transdecoder_dir/longest_orfs.pep"
TransDecoder.Predict -t "${i}" --retain_pfam_hits "${i}.domtblout" --cpu 16
done

