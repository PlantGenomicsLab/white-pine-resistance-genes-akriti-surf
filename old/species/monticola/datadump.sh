#!/bin/bash
#SBATCH --job-name=data_dump_pinus3
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=10G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
#SBATCH --partition=general
#SBATCH --qos=general 

mkdir /home/FCAM/abhattarai/SURF_work/tmp
export TMPDIR=/home/FCAM/abhattarai/SURF_work/tmp

module load sratoolkit/2.8.1 

for i in SRR1013833
do 
fastq-dump --defline-seq '@$sn[_$rn]/$ri' --split-files ${i} --gzip
done


