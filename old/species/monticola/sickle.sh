#!/bin/bash
#SBATCH --job-name=sickle_monticola
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=10G
#SBATCH -o sickle_albicaulis%j.out
#SBATCH -e sickle_albicaulis_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load sickle 


sickle pe -t sanger -f SRR1013833_1.fastq.gz -r SRR1013833_2.fastq.gz -o trimmed_SRR1013833_1.fastq -p trimmed_SRR1013833_2.fastq -l 45 -q 25 -s singles_SRR1013833.fastq



