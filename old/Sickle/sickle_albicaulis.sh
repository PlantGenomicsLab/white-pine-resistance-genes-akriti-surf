#!/bin/bash
#SBATCH --job-name=sickle_flexilis
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=10G
#SBATCH -o sickle_run_%j.out
#SBATCH -e sickle_run_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load sickle 

for i in SRR4299096 SRR4299098 SRR4299099
do 
sickle pe -t sanger -f "/home/FCAM/abhattarai/SURF_work/${i}_1.fastq.gz" -r "/home/FCAM/abhattarai/SURF_work/${i}_2.fastq.gz" -o "trimmed_${i}_1.fq.gz" -p "trimmed_${i}_2.fq.gz" -l 45 -q 25 -s "singles_${i}.fq.gz"
done

