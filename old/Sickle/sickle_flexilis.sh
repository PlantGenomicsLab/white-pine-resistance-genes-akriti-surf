#!/bin/bash
#SBATCH --job-name=sickle_flexilis
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=10G
#SBATCH -o sickle_run_%j.out
#SBATCH -e sickle_run_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load sickle 

for i in SRR10032949 SRR10032958 SRR10032961 SRR10032962 SRR10032966 SRR10032967 SRR10032969 SRR10032973 SRR10032980 SRR101383 SRR3273742
do 
sickle pe -t sanger -f "/home/FCAM/abhattarai/SURF_work/${i}_1.fastq.gz" -r "/home/FCAM/abhattarai/SURF_work/${i}_2.fastq.gz" -o "trimmed_${i}_1.fq.gz" -p "trimmed_${i}_2.fq.gz" -l 45 -q 25 -s "singles_${i}.fq.gz"
done

