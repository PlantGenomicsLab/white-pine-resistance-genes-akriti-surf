#!/bin/bash
#SBATCH --job-name=sickle_flexilis
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=10G
#SBATCH -o sickle_run_%j.out
#SBATCH -e sickle_run_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load sickle 

for i in SRR8585913 SRR8585914 SRR8585915 SRR8585916 SRR8585917 SRR8585918 SRR8585919 SRR8585920 SRR8585921 SRR8585922 SRR8585923 SRR8585939 SRR8585940 SRR8585941 SRR8585948 SRR8585949 SRR8585960 SRR8585963 SRR8585964 SRR8585965 SRR8585966
do 
sickle pe -t sanger -f "/home/FCAM/abhattarai/SURF_work/${i}_1.fastq.gz" -r "/home/FCAM/abhattarai/SURF_work/${i}_2.fastq.gz" -o "trimmed_${i}_1.fq.gz" -p "trimmed_${i}_2.fq.gz" -l 45 -q 25 -s "singles_${i}.fq.gz"
done

