#!/bin/bash
#SBATCH --job-name=sickle_run
#SBATCH --mail-user=
#SBATCH --mail-type=ALL
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=10G
#SBATCH -o sickle_run_%j.out
#SBATCH -e sickle_run_%j.err
#SBATCH --partition=general
#SBATCH --qos=general

module load sickle 

for i in ERR2040835 ERR2936763 ERR2936764 ERR2936765 ERR2936766 ERR2936767 ERR2936768 ERR2936769 ERR2936770 ERR2936771 ERR2936772
do 
sickle pe -t sanger -f "/home/FCAM/abhattarai/SURF_work/${i}_1.fastq.gz" -r "/home/FCAM/abhattarai/SURF_work/${i}_2.fastq.gz" -o "trimmed_${i}_1.fq.gz" -p "trimmed_${i}_2.fq.gz" -l 45 -q 25 -s "singles_${i}.fq.gz"
done
