#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o orthofinder%j.out
#SBATCH -e orthofinder%j.err

module load OrthoFinder/2.4.0
module unload diamond
module load diamond/0.9.25


orthofinder -f pep_files_2/ -S diamond -t 10



