### Load up those output files from orthofinder and entap
with open("/labs/Wegrzyn/Akriti_SURF/orthofinder/analysis/Orthogroups_filtered.txt", "r") as genegroups:
    ggroups = genegroups.read().splitlines()

#### calculate mean and stdev for number in each orthogroup for the angiosperm trees
#### also convert the number in each orthogroup for the neuropterans to int from string objects
import re
from operator import itemgetter
#from ast import literal_eval

# # ##### Calculate the gene lengths (aa length) for each gene
f = open("/labs/Wegrzyn/Akriti_SURF/orthofinder/pep_files_2/combined_pep.fasta", "r")
content = f.read()
lengths = []
seqList = re.split(">", content)
del seqList[0]
for seq in seqList:
    header, sequence = seq.split("\n",1)
    lengths.append([header,len(sequence)])
# sort longest genes first
sorted_lengths = sorted(lengths, key=itemgetter(1), reverse=True)
#print(sorted_lengths)

# # ### get a list of the longest gene for each orthogroup
oglong = open("orthogroup_longest2.txt" , "w")
for og in ggroups:
    genes = re.split("\t|, ", og)

    def getLongest():
        for l in sorted_lengths:
            if l[0] in genes:
                return l[0]
				
    if genes[0] != "" and genes[0] != "Orthogroup":   
        l = getLongest()
        if l is not None:
            oglong.write(genes[0] + "\t" + l + "\n")
oglong.close()
