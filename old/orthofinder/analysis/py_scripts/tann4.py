with open("/labs/Wegrzyn/Akriti_SURF/orthofinder/analysis/longest_ortho_trans.txt", "r") as f1:
    orthogroups = f1.readlines()

with open("/labs/Wegrzyn/Akriti_SURF/orthofinder/analysis/all_annotations.tsv", "r") as allannotations:
    annotations = allannotations.readlines()

def getAnnotations(anns):
    ann_columns = []
    for a in anns:
        cols = a.split("\t")
        x = [cols[0], cols[12], cols[31]]
        ann_columns.append(x)
    return ann_columns

all_anns = getAnnotations(annotations)

with open("longest_ortho_trans.txt", "r") as f2:
    orthogroups = f2.read().splitlines()

ann_out = open("annotated_orthogroups.tsv", "w")
for og in orthogroups:
    og_column = og.split(" ")
    annotated1 = []
    for ann_column in all_anns:
        #match gene names from both files
        if og_column[1] == ann_column[0]:
            annotated1 = [og_column[0]] + ann_column
            print(annotated1)
            ann_out.write("\t".join(annotated1))
            ann_out.write("\n")
ann_out.close()
f2.close()
