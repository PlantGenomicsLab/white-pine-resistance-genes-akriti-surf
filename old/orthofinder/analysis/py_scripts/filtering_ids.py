#!/usr/bin/env python
# coding: utf-8

import numpy as np
import re

with open('/labs/Wegrzyn/Akriti_SURF/orthofinder/pep_files_2/OrthoFinder/Results_Sep16_3/Orthogroups/Orthogroups.txt', 'r') as f1:
    ortho = f1.readlines()

with open('/labs/Wegrzyn/Akriti_SURF/species/combined/combined_transcriptid.txt', 'r') as f2:
    all_id = f2.readlines()

all_id_s = []
idslist = []
for i in all_id:
    x = i.rstrip('\n')
    y = x.split('\t')
    idslist.append(y)
    all_id_s.append(x)

ortho_filt = []
for group in ortho:
    for k in idslist:
        if k[0] in group:
            x = group
            ortho_filt.append(x)
            
ortho_filt = list(dict.fromkeys(ortho_filt))
len(ortho_filt)

species_count_per_orthogroup =["orthogroupID Piar Pibh Pice Pikw Pila Pist Piwl Piwn"]
for item in ortho_filt:
    ortho_split = item.split(' ')
    a = item.count('armandii')
    b = item.count('bhutanica')
    c = item.count('cembra')
    k = item.count('kwang')
    l = item.count('PILA')
    s = item.count('strobus')
    wl = item.count('walli')
    wn = item.count('wangii')
    x = (ortho_split[0] + " " + str(a) + " " + str(b) + " " + str(c) + " " + str(l) + " "+ str(k) + " " + str(s) + " " + str(wl) + " " + str(wn))
    species_count_per_orthogroup.append(x)


ortho_nlrs = []
ortho_filt_ids = []
for i in range(len(ortho_filt)):
    ortho_split = ortho_filt[i].split(' ')
    ortho_nlrs.append([ortho_split[0]])
    ortho_filt_ids.append(ortho_split[0])
    for k in idslist:
        if k[0] in ortho_filt[i]:
            ortho_nlrs[i].append(k[1])

ortho_filt_ids2 = list(dict.fromkeys(ortho_filt_ids))
    
with open('Orthogroups_filtered_ids.txt', 'w') as fn3:
    for i in ortho_filt_ids2:
        fn3.write(i + '\n')

#with open('Orthogroups_filtered_species_dist.txt', 'w') as fn1:
#    for i in species_count_per_orthogroup:
#       fn1.write(i)

#with open('Orthogroups_filtered.txt', 'w') as fn2:
#    for i in ortho_filt:
#        fn2.write(i)

