#!/bin/bash
#SBATCH --job-name=meme_l200_tnl.fa
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=akriti.bhattarai@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo xanadu-11.cam.uchc.edu
export PATH=/home/FCAM/abhattarai/meme/bin:/home/FCAM/abhattarai/meme/libexec/meme-5.2.0:/isg/shared/apps/seqtk/1.2:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

meme -protein -nmotifs 15 -minw 5 -maxw 30 -oc output_l200_tnl.fa /labs/Wegrzyn/Akriti_SURF/meme/id_lists/l200_tnl.fa
