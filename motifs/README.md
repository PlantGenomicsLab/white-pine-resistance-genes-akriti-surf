[[_TOC_]] 

# Motif Discovery?


STREME for motif discovery on 783 NLR sequences from the nature conifer paper. Default parameters were used for everything except the minimum and maximum motif widths and to stop after identifying 50 motifs to save time (the NBARC motifs should be within the top 20 motifs, set to 50 just to be sure). Motifs corresponding to the p-loop, kinase 2, GLPL, and MHD were considered "major" motifs.


```
#!/bin/bash
#SBATCH --job-name=teststreme1
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
hostname
echo -e "\nStart time:"
date

export PATH=$HOME/meme/5.2.0/bin:$HOME/meme/5.2.0/libexec/meme-5.2.0:$PATH
export PERL5LIB=$PERL5LIB:~/home/FCAM/abhattarai/perlmods/share/perl5:/home/FCAM/abhattarai/perlmods/share/perl5/Sys

streme -o test2_mon_13 --protein --minw 5 --maxw 30 --nmotifs 50 --p /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/motif_discovery/nlrs_from_paper_all_species.fa 

echo -e "\nEnd time:"
date
```

The [output motif file of STREME](streme_motifs.txt) was then edited for use with MAST for motif scanning on my transcripts to identify the start and end coordinates of the NBARC domain and complete NBARC domains.

```
#!/bin/bash
#SBATCH --job-name=mast_albicaulis
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=1G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date
export PATH=/home/FCAM/abhattarai/meme/5.2.0/bin:/home/FCAM/abhattarai/meme/5.2.0/libexec/meme-5.2.0:/home/FCAM/abhattarai/meme/bin:/home/FCAM/abhattarai/meme/libexec/meme-5.4.1:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/FCAM/abhattarai/sanity_saving_scripts:/home/FCAM/abhattarai/.local/bin:/home/FCAM/abhattarai:/home/FCAM/abhattarai/bin:/home/FCAM/abhattarai/.local/lib/python3.6/site-packages
export PERL5LIB=:~/home/FCAM/abhattarai/perlmods/share/perl5:/home/FCAM/abhattarai/perlmods/share/perl5/Sys

mast /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/streme_motifs.txt /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/get_aa_for_mast/albicaulis/nlr_trans_only_albicaulis.aa 

date
```

nbarc_from_mast.sh 

```
#!/bin/bash
#SBATCH --job-name=filter_mast
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=1G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=thewormpit@gmail.com
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
 
hostname
date
module load R
module load bedtools

/isg/shared/apps/R/4.0.3/R-4.0.3/bin/Rscript /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/get_aa_for_mast/getting_nbarc_from_mast.R mast_sectionII.txt mast_sectionI.txt 

sort nbarc_coordinates_from_mast.txt | uniq -u > nbarc_coord_dedup.txt

bedtools getfasta -fo albicaulis_nbarc_from_mast.fa -fi /core/labs/Wegrzyn/Transcriptomics/whitePines_NLRs/nlr_identification/final_transcriptomes/filtered_albicaulis_combined.aa -bed nbarc_coord_dedup.txt

date
```

The extracted NBARC sequences were then used in multiple sequence alignments as well as orthogroup assignment.