import os
import re
import numpy as np
import argparse

parser = argparse.ArgumentParser(
    prog='filtergff.py',
    usage='''python filtergff.py --gff [gff file given by InterProScan] --out [output file name]''',
    description='''this program pulls out specific entries from the gff file corresponding to features of NLR genes''')
parser.add_argument('--gff', type=str, help='the name of the gff file', required=True)
parser.add_argument('--out', type=str, help='the name for the output filtered gff file',required=True)
args=parser.parse_args()
gff=args.gff
outputname=args.out

open_gff = open(gff, "r")
content = open_gff.read()
content_list = content.split("##")
open_gff.close()
gff_only = content_list[4:-2]
fasta_only = content_list[-1:]

id_list=['PF12061', 'cd14798', 'PF18052', 'PS51153', 'PF05659', 'G3DSA:3.40.50.10140', 'PF01582', 'PS50104', 'PF13676', 'SM00255', 'SSF52200', 'G3DSA:3.40.50.300', 'SSF52540', 'G3DSA:1.10.8.430', 'PF00931', 'PR00364']
filter_list=[]
for entry in gff_only:
    x = entry
    match = False
    for ids in id_list:
        if ids in entry:
            match=True
    if match:
        filter_list.append(x)

with open(outputname, 'w') as filehandle:
    for listitem in filter_list:
        filehandle.write('##' + listitem)
