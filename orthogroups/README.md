[[_TOC_]]


# Grouping and alignment of similar NBARC regions

## Orthogroup assignment  

In order to improve alignments and generate... The extracted NBARC domains were first clustered based on sequence identity using UCLUST, however, this resulted in an excessive amount of clusters, many of which only had a handful of transcripts or were singlets. The extracted NBARC domains were split into orthogroups using Orthofinder, as it groups based on .... (sequence, functional, etc...). This resulted in a total of 25 orthogroups and the three largest orthogroups contained around 85% of all transcripts. These three largest orthogroups corresponded to the three classes of resistance gene, TNLs, RNLs, and CNLs. The TNLs were the largest with 713 transcripts, followed by the RNLs with 278 transcripts, and finally the CNLs with 114 transcripts. 

